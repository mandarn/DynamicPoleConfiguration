﻿using DynamicPoleConfiguration.Implementation;
using DynamicPoleConfiguration.Interface;
using DynamicPoleConfiguration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DynamicPoleConfiguration.Controllers
{
    public class ConfigController : Controller
    {
        IDynamicPoleConfiguration poleConfiguration = new DynamicPoleConfigurationImplementation();
        // GET: Config
        public ActionResult Index()
        {
           Config tempList = new Config();
            tempList.TempList = poleConfiguration.GetAllTemplateList();
            return View("Config", tempList);
        }

        [HttpGet]
        public JsonResult GetConfigurationData()
        {
            ConfigDataList configDataList = new ConfigDataList();
            configDataList = poleConfiguration.GetConfigurationData();
            return Json(configDataList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Saveconfiguration(AvailableTemplate _postModel)
        {
            poleConfiguration.SaveNewConfiguration(_postModel);
            return RedirectToAction("Index", "Config");
        }
    }
}