﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DynamicPoleConfiguration.Models;
using DynamicPoleConfiguration.Interface;
using DynamicPoleConfiguration.Implementation;

namespace DynamicPoleConfiguration.Controllers
{
    public class TemplateController : Controller
    {
        IDynamicPoleConfiguration poleConfiguration = new DynamicPoleConfigurationImplementation();
        // GET: Template
        public ActionResult Index()
        {
            TempList tempList = new TempList();
            tempList = poleConfiguration.GetAllTemplateList();
            return View("Template", tempList);
        }

        [HttpGet]
        public ActionResult LoadTemplate(int Id,string Description)
        {
            Session["SelectedTemplate"] = Id;
            Session["TemplateDescription"] = Description;
            return RedirectToAction("Index", "Object");
        }
    }
}