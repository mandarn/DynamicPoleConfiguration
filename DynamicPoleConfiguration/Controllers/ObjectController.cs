﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DynamicPoleConfiguration.Interface;
using DynamicPoleConfiguration.Implementation;
using DynamicPoleConfiguration.Models;

namespace DynamicPoleConfiguration.Controllers
{
    public class ObjectController : Controller
    {
        IDynamicPoleConfiguration poleConfiguration = new DynamicPoleConfigurationImplementation();
        // GET: Object
        public ActionResult Index()
        {
           
            return View("Object");
        }

        public ActionResult ShowObject(string result)
        {
            System.Object obj = new System.Object();
            obj = result;
            return View("Object", obj);
        }

        [HttpGet]
        public string GetTemplate(int Id)
        {
            string Template = poleConfiguration.GetTemplate(Id);
            return Template;
        }

        [HttpGet]
        public string GetObjectXML(string objectType)
        {
            string ObjResult = poleConfiguration.GetXmlDocType(objectType);
            return ObjResult;
        }

        [HttpPost]
        public string SaveConfigurationObject(ConfigurationObject Config)
        {
           bool status= poleConfiguration.saveConfiguration(Config);
            if(status==true)
            {
                return "Data inserted Successfully";
            }
            else
            {
                return "Unable to insert data";
            }
        }

        public ActionResult LogOut()
        {
            Session.Abandon();
           return RedirectToAction("Index", "Template");
        }
    }
}