﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DynamicPoleConfiguration.Models;

namespace DynamicPoleConfiguration.Interface
{
    public interface IDynamicPoleConfiguration
    {
        string GetXmlDocType(string objType);
        string GetTemplate(int TempId);
        bool saveConfiguration(ConfigurationObject _postModel);
        TempList GetAllTemplateList();
        ConfigDataList GetConfigurationData();
        void SaveNewConfiguration(AvailableTemplate _postModel);
    }
}