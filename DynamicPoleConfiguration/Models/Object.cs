﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DynamicPoleConfiguration.Models
{
    public class Object
    {
        public Object()
        {
            ObjectDetails = new GetObject();
           
        }
        public GetObject ObjectDetails { get; set; }
        public string ObjectResult { get; set; }
       
    }
    public class GetObject
    {
        public int Id { get; set; }
        public string ObjectName { get; set; }
        public string ObjectSchema { get; set; }
        public decimal ObjectVersion { get; set; }
        public string ObjectType { get; set; }
    }

    public class ConfigurationObject
    {
        public int ConfigId { get; set; }
        public string ConfigName { get; set; }
        public string ConfigXML { get; set; }
        public int ConfigTemplateId { get; set; }
        public decimal ConfigTemplateVersion { get; set; }
        public bool Empty { get {
                return(
                   ConfigId == 0&&
                   string.IsNullOrWhiteSpace(ConfigName)&&
                   string.IsNullOrWhiteSpace(ConfigXML)&&
                   ConfigTemplateId == 0 &&
                   ConfigTemplateVersion == 0);} }

    }
   
}