﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DynamicPoleConfiguration.Models
{
    public class Config
    {
        public Config()
        {
            TempList = new TempList();
        }
        public TempList TempList { get; set; }
    }

    public class ConfigDataList
    {
        public List<ConfigData> ConfigurationList { get; set; }
    }

    public class ConfigData
    {
        public int ConfigId { get; set; }
        public string ConfigName { get; set; }
        public int ConfigTemplateId { get; set; }
        public decimal ConfigTemplateVersion { get; set; }
        public string Details { get; set; }
    }
    public class TempList
    {
        public List<AvailableTemplate> TemplateList { get; set; }
        public int TemplateId { get; set; }
    }

    public class AvailableTemplate
    {
        public int TemplateId { get; set; }
        public string TemplateName { get; set; }
    }


}