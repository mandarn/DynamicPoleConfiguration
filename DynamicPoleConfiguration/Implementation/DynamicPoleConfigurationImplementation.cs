﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DynamicPoleConfiguration.Interface;
using DynamicPoleConfigurationEntity;
using DynamicPoleConfiguration.Models;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Data.Entity.Core.Objects;

namespace DynamicPoleConfiguration.Implementation
{
    public class DynamicPoleConfigurationImplementation:IDynamicPoleConfiguration
    {
        DynamicPoleConfigurationEntities dbContext;
        public DynamicPoleConfigurationImplementation()
        {
            dbContext = new DynamicPoleConfigurationEntities();
        }

        public string GetTemplate(int TempId)
        {
            string XMLTemplate = string.Empty;
            //var Template = dbContext.Templates.Where(t => t.TemplateNameId == TempId).FirstOrDefault();
            //if (Template != null)
            //{
                var XMLTemplateObj = dbContext.Configuration.Where(t => t.ConfigId == TempId).FirstOrDefault();
                if (XMLTemplateObj != null)
                {
                    XMLTemplate = XMLTemplateObj.ConfigXML;
                }
            //    else
            //    {
            //        XMLTemplate = Template.Template1;
            //    }
            //}
            return XMLTemplate;
        }

        public string GetXmlDocType(string objType)
        {
            var XMLType = dbContext.ConfigObject.Where(t => t.ObjectType == objType).FirstOrDefault();
            string XMLTypeString = XMLType.ObjectSchema;
            return XMLTypeString;
        }

        public GetObject GetObject()
        {
            GetObject obj = new GetObject();
            try
            {

            }
            catch (Exception e)
            {
            }
            return obj;
        }

        public bool saveConfiguration(ConfigurationObject _postModel)
        {
            try
            {
                var ConfigData = dbContext.Configuration.Where(t => t.ConfigId == _postModel.ConfigId).FirstOrDefault();
                if (ConfigData != null)
                {
                   
                    var postModel = dbContext.USP_SaveConfigurationObject("Update",ConfigData.ConfigId, _postModel.ConfigName, _postModel.ConfigXML, _postModel.ConfigTemplateId, _postModel.ConfigTemplateVersion, 1);
                    return true;                
                }
                else
                {
                    if (_postModel.ConfigId == 0)
                    {
                        var postModel =dbContext.USP_SaveConfigurationObject("Insert", 0, _postModel.ConfigName, _postModel.ConfigXML, _postModel.ConfigTemplateId, _postModel.ConfigTemplateVersion, 0);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }


            }
            catch (Exception e)
            {

            }
            return false;
        }


        //public void saveConfiguration(ConfigurationObject _postModel)
        //{
        //    try
        //    {
        //        var Flag = "";
        //        if (_postModel.ConfigId == 0)
        //        {
        //            Flag = "Insert";
        //        }
        //        else
        //        {
        //            Flag = "Update";
        //        }
        //        String strConnString = ConfigurationManager.ConnectionStrings["ConfigString"].ConnectionString;
        //        SqlConnection con = new SqlConnection(strConnString);
        //        SqlCommand cmd = new SqlCommand();
        //        SqlDataReader rdr = null;
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.CommandText = "USP_SaveConfigurationObject";
        //        cmd.Parameters.Add("@Flag", SqlDbType.VarChar, 10).Value = Flag;
        //        cmd.Parameters.Add("@ConfigId", SqlDbType.Int).Value = 0;
        //        cmd.Parameters.Add("@ConfigName", SqlDbType.VarChar, 250).Value = _postModel.ConfigName;
        //        cmd.Parameters.Add("@ConfigXML", SqlDbType.Xml).Value = _postModel.ConfigXML;
        //        cmd.Parameters.Add("@ConfigTemplateId", SqlDbType.Int).Value = _postModel.ConfigTemplateId;
        //        cmd.Parameters.Add("@ConfigTemplateVersion", SqlDbType.Decimal).Value = _postModel.ConfigTemplateVersion;
        //        cmd.Parameters.Add("@AckMsg", SqlDbType.VarChar, 250).Value = string.Empty;
        //        cmd.Connection = con;

        //        try

        //        {
        //            con.Open();
        //            rdr = cmd.ExecuteReader();
        //        }
        //        catch (Exception ex)

        //        {
        //        }

        //        finally

        //        {

        //            con.Close();

        //            con.Dispose();

        //        }

        //    }
        //    catch (Exception e)
        //    {
        //    }
        //}

        public TempList GetAllTemplateList()
        {
            TempList tempList = new TempList();
            try
            {
                tempList.TemplateList = (from TL in dbContext.Template
                                         select new AvailableTemplate
                                         {
                                             TemplateId = TL.Id,
                                             TemplateName = TL.TemplateName
                                         }).ToList();
            }
            catch (Exception ex)
            {
            }
            return tempList;
        }

        public ConfigDataList GetConfigurationData()
        {
            ConfigDataList configDataList = new ConfigDataList();
            try
            {
                configDataList.ConfigurationList = (from CL in dbContext.Configuration
                                                    select new ConfigData
                                                    {
                                                        ConfigId = CL.ConfigId,
                                                        ConfigName = CL.ConfigName,
                                                        ConfigTemplateId = CL.ConfigTemplateId,
                                                        ConfigTemplateVersion = CL.ConfigTemplateVersion,
                                                        Details ="<i class='fa fa-eye btnSelect'></i>"
                                                    }).ToList();
            }
            catch (Exception e)
            {

                throw;
            }
            return configDataList;
        }

        public void SaveNewConfiguration(AvailableTemplate _postModel)
        {
            try
            {
                var SelectedTemplate = dbContext.Template.Where(t => t.Id == _postModel.TemplateId).FirstOrDefault();
                if (SelectedTemplate != null)
                {
                    DynamicPoleConfigurationEntity.Configuration configuration = new DynamicPoleConfigurationEntity.Configuration();

                    configuration.ConfigName = _postModel.TemplateName;
                    configuration.ConfigTemplateId = SelectedTemplate.Id;
                    configuration.ConfigXML = SelectedTemplate.Template1;
                    dbContext.Entry(configuration).State = System.Data.Entity.EntityState.Added;
                    dbContext.SaveChanges();
                }
            }
            catch (Exception e)
            {
            }
        }
    }
}