﻿function OnlyString(ControlValue, ControlId) {
       if (ControlValue.length > 0) {
           var StringValidation = /^[A-Za-z0-9]+$/;
        if (StringValidation.test(ControlValue)) {
            return true;
        }
        else {
            alert("Enter Valid Text");
            $('#' + ControlId).val('');
            return false;
        }
    }
    
}

function OnlyNumeric(ControlValue, ControlId) {
    if (ControlValue.length > 0) {
        var NumericValidation = /[0-9]$/;
        if (NumericValidation.test(ControlValue)) {
            return true
        }
        else {
            alert("Enter Valid Number");
            $('#' + ControlId).val('');
            return false;
        }
    }
    
}
//Validation for Email
function validateEmail(ControlValue) {
    //var value1 = $('#txtEmail').val();
    if (ControlValue.length > 0) {
        var regex = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
        if (!regex.test(ControlValue)) {
            alert("Enter valid email");
            $('#' + ControlValue).val('');
            return false;
        }
        else {
            return true;
        }
    }

}


