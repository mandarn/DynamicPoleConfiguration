﻿var menuToggle = true;

$(document).ready(function () {


    // Init Tabs
    $("#ipsg-tabs").tabs();

    // Init Tooltips
    $(function () {
        $('.ipsg-tab').tooltip();
    });;

    // Init Menu 
    accordionMenu();
    initLeftNav();

    // Toggle Menu
    $('.ipsg-logo-menu-btn').click(function () {

        if (menuToggle == false) {
            menuToggle = true;
            $('.sg-accordion > .ipsg-nav-items').hide();
            $('.sg-accordion h3').removeClass('state-selected');
            $('.ipsg-container').addClass('ipsg-menu-collapse');
        } else {
            menuToggle = false;
            $('.ipsg-container').removeClass('ipsg-menu-collapse');
        }

        Cookies.set("navState", menuToggle);
    });

});

function initLeftNav() {
    var cookieValue = Cookies.get("navState");

    if (cookieValue == null) {
        cookieValue = 'false';
    }

    if (cookieValue == 'true') {
        menuToggle = true;
        $('.sg-accordion > .ipsg-nav-items').hide();
        $('.sg-accordion h3').removeClass('state-selected');
        $('.ipsg-container').addClass('ipsg-menu-collapse');
    } else {
        menuToggle = false;
        $('.ipsg-container').removeClass('ipsg-menu-collapse');
    }

}



function accordionMenu() {
    var allPanels = $('.sg-accordion > .ipsg-nav-items');
    var menuLeaving = false;
    $('.sg-accordion > h3 > a').click(function () {
        $('.ipsg-container').removeClass('ipsg-menu-collapse');
        menuToggle = false;
        if (!menuToggle) {
            if (!$(this).parent().hasClass('state-selected')) {
                allPanels.slideUp(600);
                $('.sg-accordion h3').removeClass('state-selected');

                $(this).parent().next().slideDown(600);
                $(this).parent().addClass('state-selected');
            } else {
                allPanels.slideUp(600);
                $('.sg-accordion h3').removeClass('state-selected');
            }
        }

        return false;
    });

    //$('.sg-accordion > h3 > a').mouseenter(function () {
    //    if (menuToggle) {
    //        menuLeaving = false;
    //        $('.sg-accordion > .ipsg-nav-items').hide();
    //        $('.sg-accordion h3').removeClass('state-selected');
    //        $(this).parent().next().show();
    //        $(this).parent().addClass('state-selected');
    //    }
    //});

    //$('.sg-accordion > .ipsg-nav-items').mouseenter(function () {
    //    if (menuToggle) {
    //        menuLeaving = false;
    //    }
    //});

    //$('.sg-accordion > h3 > a').mouseleave(function () {
    //    if (menuToggle) {
    //        menuLeaving = true;

    //        setTimeout(function () {
    //            if (menuLeaving) {
    //                $('.sg-accordion > .ipsg-nav-items').hide();
    //                $('.sg-accordion h3').removeClass('state-selected');
    //            }
    //        }, 400);
    //    }
    //});

    //$('.sg-accordion > .ipsg-nav-items').mouseleave(function () {
    //    if (menuToggle) {
    //        menuLeaving = true;

    //        setTimeout(function () {
    //            if (menuLeaving) {
    //                $('.sg-accordion > .ipsg-nav-items').hide();
    //                $('.sg-accordion h3').removeClass('state-selected');
    //            }
    //        }, 400);
    //    }
    //});
}



//var menuToggle = false;

//$(document).ready(function () {


//    // Init Tabs
//    $("#ipsg-tabs").tabs();

//    // Init Tooltips
//    $(function () {
//        $('.ipsg-tab').tooltip();
//    });;

//    // Init Menu 
//    accordionMenu();
//    initLeftNav();

//    //$('.ipsg-main-nav').mouseenter(function () {      
//    //    $('.ipsg-container').removeClass('ipsg-menu-collapse');
//    //})

//    //$('.ipsg-main-nav').mouseleave(function () {
//    //    $('.ipsg-container').addClass('ipsg-menu-collapse');

//    //})

//    var timer;
//    $('.ipsg-main-nav').on({
//        mouseleave: function () {            
//            clearTimeout(timer);
//            timer = setTimeout(function () {
//                $('.ipsg-container').addClass('ipsg-menu-collapse');
//            }, 100)
//        },
//        mouseenter: function () {           
//            setTimeout(function () {               
//                    $('.ipsg-container').removeClass('ipsg-menu-collapse');                
//            }, 100);
//        }
//    });

//    // Toggle Menu
//    //$('.ipsg-logo-menu-btn').click(function () {

//    //    if (menuToggle == false) {
//    //        menuToggle = true;
//    //        $('.sg-accordion > .ipsg-nav-items').hide();
//    //        $('.sg-accordion h3').removeClass('state-selected');
//    //        $('.ipsg-container').addClass('ipsg-menu-collapse');
//    //    } else {
//    //        menuToggle = false;
//    //        $('.ipsg-container').removeClass('ipsg-menu-collapse');
//    //    }

//    //    Cookies.set("navState", menuToggle);        
//    //});

//});

//function initLeftNav() {
//    //var cookieValue = Cookies.get("navState");

//    //if (cookieValue == null) {
//    //    cookieValue = 'false';
//    //}

//    //if (cookieValue == 'true') {
//        //menuToggle = true;
//        $('.sg-accordion > .ipsg-nav-items').hide();
//        $('.sg-accordion h3').removeClass('state-selected');
//        $('.ipsg-container').addClass('ipsg-menu-collapse');
//    //} else {
//    //    menuToggle = false;
//    //    $('.ipsg-container').removeClass('ipsg-menu-collapse');
//    //}

//}

//function accordionMenu() {
//    var allPanels = $('.sg-accordion > .ipsg-nav-items');    
//    var menuLeaving = false;
//    $('.sg-accordion > h3 > a').click(function () {
//        if (!menuToggle) {
//            if (!$(this).parent().hasClass('state-selected')) {
//                allPanels.slideUp('fast');
//                $('.sg-accordion h3').removeClass('state-selected');

//                $(this).parent().next().slideDown('fast');
//                $(this).parent().addClass('state-selected');
//            } else {
//                allPanels.slideUp('fast');
//                $('.sg-accordion h3').removeClass('state-selected');
//            }
//        }

//        return false;
//    });

//    //$('.sg-accordion > h3 > a').mouseenter(function () {
//    //    //if (menuToggle) {
//    //        menuLeaving = false;
//    //        $('.sg-accordion > .ipsg-nav-items').hide();
//    //        $('.sg-accordion h3').removeClass('state-selected');
//    //        $(this).parent().next().show();
//    //        $(this).parent().addClass('state-selected');
//    //    //}
//    //});

//    //$('.sg-accordion > .ipsg-nav-items').mouseenter(function () {
//    //    //if (menuToggle) {
//    //        menuLeaving = false;
//    //    //}
//    //});

//    //$('.sg-accordion > h3 > a').mouseleave(function () {
//    //    //if (menuToggle) {
//    //        menuLeaving = true;

//    //        setTimeout(function () {
//    //            if (menuLeaving) {
//    //                $('.sg-accordion > .ipsg-nav-items').hide();
//    //                $('.sg-accordion h3').removeClass('state-selected');
//    //            }
//    //        }, 400);
//    //    //}
//    //});

//    //$('.sg-accordion > .ipsg-nav-items').mouseleave(function () {
//    //    //if (menuToggle) {
//    //        menuLeaving = true;

//    //        setTimeout(function () {
//    //            if (menuLeaving) {
//    //                $('.sg-accordion > .ipsg-nav-items').hide();
//    //                $('.sg-accordion h3').removeClass('state-selected');
//    //            }
//    //        }, 400);
//    //    //}
//    //});
//}