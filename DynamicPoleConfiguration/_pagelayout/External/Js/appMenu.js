﻿function getApplications(successCallback, failureCallback) {

    $.ajax({
        method: 'GET',
        url: 'https://testing.web.sso.ipsmetersystems.com/Profile/UserApplications',
        xhrFields: {
            withCredentials: true
        },
        success: function (result) {
            successCallback(result);
        },
        error: function (xhr, status, error) {
            failureCallback();
        }
    });

}

$("#appMenuLogo").click(function () {

    getApplications(function (result) {

        var $appMenuContainer = $("#appMenuContainer");

        var content = '';
        content += '<div id="appMenu" style="background-color: #004F5B; padding-bottom: 10px;">';
        content += ' <div style="color: white; font-weight: bold; padding-top: 10px; padding-left: 10px">';
        content += '  <span>';
        content += '   <span style="float: right; padding-right: 10px; vertical-align: middle;">';
        content += '    <button type="button" class="close" style="color: white;" aria-label="Close" onclick="closeAppMenu()">';
        content += '     <span aria-hidden="true">&times;</span>';
        content += '    </button>';
        content += '   </span>';
        content += '   <h4><span style="text-align:left; width: 100%">IPS PARKING MANAGEMENT SUITE</span></h4>';
        content += '  </span>';
        content += ' </div>';
        content += ' <div style="margin-top: 30px; margin-left: 20px; margin-right: 20px; color: white;">';

        content += '  <h5>MY APPS</h5>';
        content += '  <hr style="width:100%;" />';


        content += '<div class="row">';
        $.each(result, function (i, v) {

            if (v.IsEnabled) {

                content += '<div class="col-xs-12 col-lg-4 col-md-6 col-sm-6" style="text-align: center; cursor: pointer;">';
                content += ' <div style="margin: 10px 0px 0px 0px; height: 180px; cursor: pointer;" >';
                content += '  <div style="background: white; height: 180px; padding-top: 20px; border: 1px solid grey; cursor: pointer;" onclick="navToApp(\'' + v.ApplicationUrl + '\')">';
                content += '   <div style="min-height: 90%; cursor: pointer; color: ' + v.ApplicationDisplayBackgroundColor + ';">';
                content += '    <img src="' + v.ApplicationIconColor + '" height="30px" width="30px" />';
                content += '    <div style="padding-top: 20px; cursor: pointer;"><h4>' + v.ApplicationDisplayFullName + '</h4></div>';
                content += '   </div>';
                content += '  </div>';
                content += ' </div>';
                content += '</div>';

            }

        });
        content += '</div>';



        content += '  <h5>OTHER IPS APPS</h5>';
        content += '  <hr style="width:100%;" />';


        content += '<div class="row">';
        $.each(result, function (i, v) {

            if (!v.IsEnabled) {

                content += '<div class="col-xs-12 col-lg-4 col-md-6 col-sm-6" style="text-align: center;">';
                content += ' <div style="margin: 10px 0px 0px 0px; height: 180px;">';
                content += '  <div style="background: white; height: 180px; padding-top: 20px; border: 1px solid grey; background-color: grey;">';
                content += '   <div style="min-height: 90%; color: black; background-color: grey;">';
                content += '    <img src="' + v.ApplicationIcon + '" height="30px" width="30px" />';
                content += '    <div style="padding-top: 20px;"><h4>' + v.ApplicationDisplayFullName + '</h4></div>';
                content += '   </div>';
                content += '  </div>';
                content += ' </div>';
                content += '</div>';

            }

        });
        content += '</div>';
        content += '</br>';



        content += ' </div>';
        content += '</div>';
        $appMenuContainer.html(content);


        $appMenuContainer.slideDown();

    },
        function () {

        }
    );

})


function navToApp(url) {
    window.location.href = url;
}

function closeAppMenu() {
    $("#appMenuContainer").slideUp();
}