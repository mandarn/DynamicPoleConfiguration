﻿function getApplications(successCallback, failureCallback) {

    $.ajax({
        method: 'GET',
        url: 'https://dev.web.sso.ipsmetersystems.com/Profile/UserApplications',
        xhrFields: {
            withCredentials: true
        },
        success: function (result) {
            successCallback(result);
        },
        error: function (xhr, status, error) {
            failureCallback();
        }
    });

}



$(function () {

    getApplications(function (result) {

        var content = '';
        content += '<div><div class="row">';
        $.each(result, function (i, v) {

            content += '<div class="col-xs-12 col-lg-4 col-md-4" style="margin:20px;">';
            content += '<table><tr><td style="text-align:center; width: 50%;"><a href="' + v.ApplicationUrl + '"><img height="44" weight="44" src="' + v.ApplicationIcon + '" alt="' + v.ApplicationName + '" /></a></td></tr><tr><td style="text-align:center;"><h5><div style="white-space:nowrap;" />' + v.ApplicationName + '</div></h5></td></tr></table>';
            content += '</div>';

        });
        content += "</div></div>";

        $("#appMenuLogo").popover(
            {
                placement: "bottom",
                animation: "true",
                content: content,
                trigger: "click",
                delay: { show: 100, hide: 100 },
                container: 'body',
                title: '<span class="text-info">&nbsp;</span>' +
                        '<button type="button" id="close" class="close" onclick="$(&quot;#appMenuLogo&quot;).popover(&quot;hide&quot;);">&times;</button>',
                html: true
            }
        );

    },
    function () {

    })

});
