﻿$(function () {
    $("#side-menu").metisMenu();

    var ajaxSubmitModal = $(".modal.ajax-submit");
    if (0 !== ajaxSubmitModal.length) {
        ajaxSubmitModal.find("form").validate().settings.submitHandler = function (form) {
            var buttons = $(form).find(".modal-footer button");

            // disable the buttons temporarily
            buttons.prop("disabled", true);

            $.ajax({
                url: $(form).prop("action"),
                method: $(form).prop("method"),
                data: $(form).serialize()
            }).done(function (data, textStatus, jqXHR) {
                if (data.IsSuccessful) {
                    $(form).closest(".modal").modal("hide");
                    $.growl.notice({ message: "The record was saved successfully.", location: "br" });
                }
                else {
                    // todo
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                // todo
            }).always(function () {
                // enable buttons
                buttons.prop("disabled", false);
            });
        };

        ajaxSubmitModal.on("shown.bs.modal", function () {
            ajaxSubmitModal.find("form").validate();
        });

        ajaxSubmitModal.on("hidden.bs.modal", function () {
            var form = ajaxSubmitModal.find("form");
            var validator = form.validate();

            // remove validation error class
            form.find(":input").each(function () { $(this).removeClass("input-validation-error"); });

            // hide error labels
            form.find(".field-validation-error span").each(function () { validator.settings.success($(this)); });

            // reset form
            validator.resetForm();
            form[0].reset();
        });
    }

    $("time").each(function (i, v) {
        var time = moment($(v).data("datetime"));
        var format = $(v).data("format");

        if (format === "date") {
            $(v).text(time.format("M/D/YYYY"));
        }
        else if (format === "time") {
            $(v).text(time.format("hh:mm A"));
        }
        else if (format === "datetime") {
            $(v).text(time.format("M/D/YYYY hh:mm A"));
        }
    });
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function () {
    $(window).bind("load resize", function () {
        var topOffset = 75;
        var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $("div.navbar-collapse").addClass("collapse");
            //topOffset = 100; // 2-row-menu
        } else {
            $("div.navbar-collapse").removeClass("collapse");
        }

        var height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    var element = $("ul.nav a").filter(function () {
        if (this.href === url.toString()
            || (url.href.indexOf(this.href) === 0 && this.href !== url.origin + "/")
            || $(this).hasClass("active")) {
            return true;
        }

        return false;
    }).addClass("active").parent().parent().addClass("in").parent();

    if (element.is("li")) {
        element.addClass("active");
    }
});
