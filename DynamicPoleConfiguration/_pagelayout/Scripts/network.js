﻿
function loginPost(userName, password, token, recaptchaValue,
                   successCallback, failureCallback) {

    $.ajax({
        method: 'POST',
        url: API_BASE_ADDRESS + 'Profile/Login',
        data: {
            __RequestVerificationToken: token,
            UserName: userName,
            Password: password,
            RecaptchaValue: recaptchaValue
        },
        success: function (result) {
            successCallback(result);
        },
        error: function (xhr, status, error) {
            failureCallback();
        }
    });

}

function resetPassword(userName, recaptchaValue, successCallback, failureCallback)
{

    var gResponse = grecaptcha.getResponse();
    var resetPasswordInfo = {
        UserName: userName,
        RecaptchaValue: recaptchaValue
    }

    var data = JSON.stringify(resetPasswordInfo);

    $.ajax({
        method: 'POST',
        url: API_BASE_ADDRESS + 'Profile/ResetPassword',
        data: data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            successCallback(result);
        },
        error: function (xhr, status, error) {
            window.location = API_BASE_ADDRESS
        }
    });

}

function checkResetPasswordTokenValidity(resetToken, successCallback, failureCallback) {

    var resetTokenValidInfo = {
        ResetPasswordToken: resetToken
    }

    var data = JSON.stringify(resetTokenValidInfo);

    $.ajax({
        method: 'POST',
        url: API_BASE_ADDRESS + 'Profile/CheckResetPasswordToken',
        data: data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            successCallback(result);
        },
        error: function (xhr, status, error) {
            window.location = API_BASE_ADDRESS
        }
    });

}

function changePassword(oldPassword, newPassword, confirmPassword, validationToken, successCallback, failureCallback) {

    var changePasswordInfo = {
        oldPassword: oldPassword,
        newPassword: newPassword,
        confirmPassword: confirmPassword,
        validationToken: validationToken
    };

    var data = JSON.stringify(changePasswordInfo);

    $.ajax({
        method: 'PUT',
        url: API_BASE_ADDRESS + 'Profile/Password',
        data: data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            successCallback(result);
        },
        error: function (xhr, status, error) {
            failureCallback();
        }
    });

}