﻿var API_BASE_ADDRESS = 'http://localhost:51790/';

var FADE_LENGTH = 2000;
var FADE_OUT_TIME = 10000;

var QUERY_STRING_RESET_PASSWORD_KEY = 'resetPassword';
var QUERY_STRING_PASSWORD_RESET_VALIDATION_TOKEN_KEY = 'validationToken';

var RECAPTCHA_SITE_KEY = '6Lf5bB0UAAAAAK-wOYc-ItQSdl0Wud0H6c7eZPze';
var RECAPTCHA_THEME = 'light';

var recaptchaForgotPasswordConfirm = null;
var recaptchaLogin = null;

var cPosition = false;

function gCaptchaOnloadCallback() {

    if ($('#recaptchaForgotPasswordConfirm').length > 0)
        recaptchaForgotPasswordConfirm = grecaptcha.render('recaptchaForgotPasswordConfirm', {
            'sitekey': RECAPTCHA_SITE_KEY,
            'theme': RECAPTCHA_THEME
        });

    if (isIPad()) {

        $('#recaptchaForgotPasswordConfirm').on('touchstart touchmove touchend', function (e) {
            e.stopPropagation();
        });

    }





    if ($('#recaptchaLogin').length > 0)
        recaptchaLogin = grecaptcha.render('recaptchaLogin', {
            'sitekey': RECAPTCHA_SITE_KEY,
            'theme': RECAPTCHA_THEME
        });

    if (isIPad()) {

        $('#recaptchaLogin').on('touchstart touchmove touchend', function (e) {
            e.stopPropagation();
        });

    }

}

function getParameterByName(name) {
    url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function isComplex(strValue) {
    return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/.test(strValue);
}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function GetJSDateFromMS(str) {
    return eval("new " + str.replace(/\//g, ""));
}

function setCookie(cname, cvalue, date) {
    $.removeCookie(cname, { path: '/', expires: -1 });
    var expires = "expires=" + date.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/;";

}

function isMobileBrowser() {
    if (/Android|webOS|iPhone|iPod|iPad|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))
        return true;
    else
        return false;
}

function isIPad() {
    if (/iPad/i.test(navigator.userAgent))
        return true;
    else
        return false;
}