﻿function BindSubCategory() {

    $.ajax({
        type: "Get",
        url: Config.url + 'Report/AdvanceSearch/BindSubCategory?categoryID=' + $("#Params_Category_CategoryId").val(),
        dataType: 'json',
        accept: 'application/json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            var data = response;
            var htmltext = "";
            htmltext += "<option value=''>All</option>";
            for (var i = 0; i < response.length; i++) {
                htmltext += "<option value='" + response[i].SubcategoryID + "'>" + response[i].SubcategoryDescription + "</option>";
            }
            $("#Params_SubCategory_SubCategoryId").html(htmltext)
        },
        error: function (xhr, status, error) {
            $("#Ips_loader_div").hide();
            //console.log(xhr.responseText)
            var err = eval("(" + xhr.responseText + ")");
            throw err;
        }
    });
}

$(document).ready(function () {

    $("#checkAll").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });

});

function closeDiv(divId) {
    $("#" + divId).hide();
}

function _SetValue(ev) {
    if (ev.checked == true) {
        document.getElementById("OutPut_Isminimum").checked = true;
    }
    else {
        document.getElementById("OutPut_Isminimum").checked = false;
    }
}