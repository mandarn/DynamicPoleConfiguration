﻿$(document).ready(function () {
    $("#Ips_loader_div").show();
    $.ajax({
        type: "Get",
        url: Config.url + 'Transaction/RMA/JsonDetailList',
        dataType: 'json',
        accept: 'application/json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            //console.log(response);
            data = response;
            setTimeout(function () { createSimpleFilteringGrid(); }, 500);
        },
        error: function (xhr, status, error) {
            $("#Ips_loader_div").hide();
            //console.log(xhr.responseText)
            var err = eval("(" + xhr.responseText + ")");
            throw err;
        }
    });
    //
});

function createSimpleFilteringGrid() {
    $("#gridSimpleFiltering").igGrid({
        autoGenerateColumns: false,
        columns: [
            { headerText: "PartReceiptDetailID", key: "PartReceiptDetailID", dataType: "int", hidden: true },
            { headerText: "PartReceiptDetailIDAse", key: "PartReceiptDetailIDAse", dataType: "int", hidden: true },
            { headerText: "Part Number", key: "PartNumber", dataType: "string" },
            { headerText: "Part Transferred", key: "QuantityTransferred", dataType: "string" },
            { headerText: "Stock Area", key: "StockAreaDescription", dataType: "string" },
            { headerText: "Action", key: "Action", template: "<a href='" + Config.url + "Transaction/RMA/RMAOrderPartEdit/${PartReceiptDetailIDAse}' class='dwn_btnt'><img title='Edit' src='" + Config.url + "Client/img/Edit.png'></a> <a href='" + Config.url + "Transaction/RMA/RMAOrderPartDelete/${PartReceiptDetailIDAse}' class='dwn_btnt'><img title='Delete' src='" + Config.url + "Client/img/delete.png'></a>", width: "12%", filter: "disable" }

        ],
        dataSource: data,
        renderCheckboxes: true,
        responseDataKey: "results",
        features: [
            {
                name: "Filtering",
                type: "local",
                mode: "simple",
                filterDialogContainment: "window",
                columnSettings: [
                {
                    columnKey: "PurchaseOrderNumber",
                    condition: "startsWith"

                },
                { columnKey: "Action", allowFiltering: false }
                ]

            },
            {
                name: "Paging",
                type: "local",
                pageSize: 10
            }
        ]
    });
    $("#Ips_loader_div").hide();
}