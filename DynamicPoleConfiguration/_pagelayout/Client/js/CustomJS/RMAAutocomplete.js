﻿$(function () {

    var _VendorName = $("#VendorId");
    var _PurchaseOrderNumber = $("#RMAOrderNumber");
    var _PartNumber = $("#PartNumber");
    var _QunatityGet = $("#QunatityGet");
    var _QunatityRemain = $("#QunatityRemain");
    var _StockAreaDescription = $("#StockAreaDescription");
       
//////////////////////////////////////////////////////////////////////////////////////////////////////
    _StockAreaDescription.change(function () {
        var StockAreaDescription = _StockAreaDescription.val();
         var PartNumber = _PartNumber.val();
         if (PartNumber != "" && PartNumber != null && PartNumber != undefined) {
             if (StockAreaDescription != "" && StockAreaDescription != null && StockAreaDescription != undefined) {
                 $("#Ips_loader_div").show();
                 checkStockDetail();
             }
         } else {
             _StockAreaDescription.prop('selectedIndex', 0);
             $("#stock_error").show();
         }
    });

//////////////////////////////////////////////////////////////////////////////////////////////////

    _VendorName.change(function () {
        $("#Vendor_error").hide();
    });

/////////////////////////////////////////////////////////////////////////////////////////////////////

    _PurchaseOrderNumber.keyup(function () {
        $("#ordernumber_error").hide();
        $("#part_btn").attr("disabled", false);
        $(".input_disable").attr("disabled", false);
        $("#ReceiptDate").val('');
    });

    _PurchaseOrderNumber.blur(function () {
        $("#ordernumber_error").hide();       
        var VendorName = _VendorName.val();
        ////alert(VendorName);
        if (VendorName != "" && VendorName != null && VendorName != undefined) {
            var PurchaseOrderNumber = _PurchaseOrderNumber.val();
            if (PurchaseOrderNumber != "") {
                $("#Ips_loader_div").show();
                setTimeout(function () { getOrdernumber(); }, 500);
            }
        } else {
            $("#Vendor_error").show();
        }
    });

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    _PartNumber.keyup(function () {
        $("#partnumber_error").hide();
        $("#part_btn").attr("disabled", false);
        $(".input_disable").attr("disabled", false);
        $("#stock_error").hide();
    });

    _PartNumber.blur(function () {
        var getCount = $(".ui-autocomplete li").length;
        if (getCount == 1) {
            $("#PartNumber").val($(".ui-autocomplete li").html());
        }
        else {
            $("#partnumber_error").hide();
            var PartNumber = _PartNumber.val();
            if (PartNumber != "" && PartNumber != null && PartNumber != undefined) {
                $("#Ips_loader_div").show();
                setTimeout(function () { getPartnumber(); }, 500);
            }
        }
    });

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //QunatityGet validation
    _QunatityGet.keyup(function () {
        var QunatityRemain = _QunatityRemain.val();
        var QunatityGet = _QunatityGet.val();        
        var _status = (Number(QunatityGet) <= Number(QunatityRemain));
        var _zero = (Number(QunatityGet) > Number(0));
        if (_zero == true) {
                if (_status == true) {
                    _QunatityGet.val(QunatityGet);
                }else{
                    _QunatityGet.val('');
                }
        } else {
            _QunatityGet.val('');
        }
        
    });
  

////////////////////////////////    //////////Auto complete box///////////////////////////////////////////////////////////////////////////////////
    var getData = function (request, response) {
        $.getJSON(
            Config.url + 'Transaction/Purchased/AutocompleteVendorName?name=' + request.term,
            function (data) {
                response(data);
            });
    };

    $(".autocomplete").autocomplete({
        source: getData
    });
    
});

/////////////////////////////////////////////////////////////////////////////////////////////////////

function getOrdernumber() {
    var turl = Config.url + 'Transaction/RMA/CheckOrderNumber';
    var PurchaseOrderNumber = $("#RMAOrderNumber").val();
    var Vendor = $("#VendorId").val();    
    if (Vendor != "" && Vendor != null && Vendor != undefined) {
            if (PurchaseOrderNumber != "" && PurchaseOrderNumber != null && PurchaseOrderNumber != undefined) {
                $.ajax({
                    type: "GET",
                    url: turl,
                    data: { number: PurchaseOrderNumber, Vendor: Vendor },
                    async: false,
                    dataType: 'json',
                    success: function (data) {
                        if (data == true) {
                            $("#ordernumber_error").show();
                            $(".input_disable").attr("disabled", true);
                            $("#part_btn").attr("disabled", true);
                        }
                    }
                });
            }
        }
    $("#Ips_loader_div").hide();
}

function getPartnumber() {
    var turl = Config.url + 'Transaction/RMA/CheckPartNumber';
    var _PartNumber = $("#PartNumber").val();
    if (_PartNumber != "" && _PartNumber != null && _PartNumber != undefined) {
        $.ajax({
            type: "GET",
            url: turl,
            data: { number: _PartNumber },
            async: false,
            dataType: 'json',
            success: function (data) {
               
                if (data == true) {
                    $("#partnumber_error").show();
                    $(".input_disable").attr("disabled", true);
                    $("#part_btn").attr("disabled", true);
                    $("#QuantityRequested").val('0');
                    $("#QunatityRemain").val('0');
                    $("#QunatityGet").val('0');
                    //getPartCalculation();
                } else {
                    getPartCalculation();
                }
            }
        });
    }
    $("#Ips_loader_div").hide();
}

function getPartCalculation() {
    var turl = Config.url + 'Transaction/RMA/PartCalculation';
    var _PurchaseNumber = $("#RMAOrderNumber").val();
    var _PartNumber = $("#PartNumber").val();

    if (_PartNumber != "" && _PartNumber != null && _PartNumber != undefined && _PurchaseNumber != "" && _PurchaseNumber != null && _PurchaseNumber != undefined) {
        $.ajax({
            type: "GET",
            url: turl,
            data: { number: _PurchaseNumber, part: _PartNumber  },
            async: false,
            dataType: 'json',
            success: function (data) {
                console.log(data);
                if (data != null) {
                    $("#QuantityRequested").val('0').val(data.TotalCount);
                    $("#QunatityRemain").val('0').val(data.UsedCount);
                    if (data.RemainCount == 0) { var _remain = ''; } else { var _remain = data.RemainCount; }
                    $("#QunatityGet").val('0').val(_remain);
                }               
            }
        });
    }   
}

function checkStockDetail() {
    var turl = Config.url + 'Transaction/RMA/checkStockDetail';
    var PartNumber = $("#PartNumber").val();
    var StockAreaDescription = $("#StockAreaDescription").val();
    if (PartNumber != "" && PartNumber != null && PartNumber != undefined) {
        if (StockAreaDescription != "" && StockAreaDescription != null && StockAreaDescription != undefined) {
            $.ajax({
                type: "GET",
                url: turl,
                data: { number: PartNumber, StockAreaDescription: StockAreaDescription },
                async: false,
                dataType: 'json',
                success: function (data) {
                    if (data == true) {                       
                        $("#part_btn").attr("disabled", true);
                        alert("Please change stock area!");
                    }else{$("#part_btn").attr("disabled", false);}
                }
            });
        }
    }
    $("#Ips_loader_div").hide();
}








