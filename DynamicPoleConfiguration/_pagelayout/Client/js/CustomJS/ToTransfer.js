﻿$(function () {

    //////set the value
    var ToQuantity = $("#ToQuantity");
    var FromQuantity = $("#FromQuantity");
    var PartNumber = $("#PartNumber");

    ///change the drop down
    $("#Fromstockarea").change(function () {
        ToQuantity.val('');
        FromQuantity.val('');
        PartNumber.val('');
        $("#Ips_loader_div").show();
        setTimeout(function () { ToDropdownlist(); }, 2000);
    });

    $("#PartNumber").keyup(function () {
        ToQuantity.val('');
        FromQuantity.val('');        
    });

    $("#PartNumber").blur(function () {
        var _PartNumber = $("#PartNumber").val();
        if (_PartNumber != "" && _PartNumber != null && _PartNumber != undefined) {
            $("#Ips_loader_div").show();
            setTimeout(function () { getTransferCalculation(); }, 2000);
        }
    });     

    //QunatityGet validation
    $("#ToQuantity").keyup(function () {
        var _zerostatus = $("#ToQuantity").val();
        var _Zero = (Number(_zerostatus) > Number(0));
        if (_Zero == true) {
            var _FromQuantity = $("#FromQuantity").val();
            var _ToQuantity = $("#ToQuantity").val();
            var _status = (Number(_ToQuantity) <= Number(_FromQuantity));
            console.log(_FromQuantity);
            //alert(_status);
            if (_status == true) {
                $("#ToQuantity").val(_ToQuantity);
            } else {
                $("#ToQuantity").val('');
            }
        } else {
            $("#ToQuantity").val('');
        }         
    });
   
});

function ToDropdownlist() {
    var Id = $("#Fromstockarea").val();
    $.ajax({
        type: "Get",
        url: Config.url + 'Transaction/Transfer/ToStockList',
        data: { StockId: Id },
        dataType: 'json',
        accept: 'application/json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            var dropdown = $('#Tostockarea');
            dropdown.empty();             
            $.each(response, function (index, value) {
            //console.log(value);                
                $("<option>").val(value.StockAreaId).text(value.StockAreaDescription).appendTo('#Tostockarea');
            });
        },
        error: function (xhr, status, error) {
            $("#Ips_loader_div").hide();
            //console.log(xhr.responseText)
            var err = eval("(" + xhr.responseText + ")");
            throw err;
        }
    });
    $("#Ips_loader_div").hide();
}

function getTransferCalculation() {

    var turl = Config.url + 'Transaction/Transfer/TranferCaluclation';
    var _FromStockId = $("#Fromstockarea").val();
    var _ToStockId = $("#Tostockarea").val();
    var _PartNumber = $("#PartNumber").val();

    if (_PartNumber != "" && _PartNumber != null && _PartNumber != undefined && _FromStockId != "" && _FromStockId != null && _FromStockId != undefined) {
        $.ajax({
            type: "GET",
            url: turl,
            data: { Partnumber: _PartNumber, FromStockId: _FromStockId, ToStockId: _ToStockId },
            async: false,
            dataType: 'json',
            success: function (data) {
                if (data != null) {
                    $("#PurchaseOrderNumber").val(data.PurchaseOrderNumber);
                    $("#FromQuantity").val(data.FromQuantity);
                    $("#VendorName").val(data.VendorName);
                } else {
                    $("#PurchaseOrderNumber").val('');
                    $("#FromQuantity").val('');
                    $("#VendorName").val('');
                }
                
            }
        });
    }
    $("#Ips_loader_div").hide();
}



