﻿$(document).ready(function () {
    $("#Ips_loader_div").show();
    var Id = $("#StockRackIdAse").val();
    $.ajax({
        type: "Get",
        url: Config.url + 'Administration/StockArea/BinJsonList',
        data: { RackId: Id },
        dataType: 'json',
        accept: 'application/json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            //console.log(response);
            data = response;
            setTimeout(function () { createSimpleFilteringGrid(); }, 500);
        },
        error: function (xhr, status, error) {
            $("#Ips_loader_div").hide();
            //console.log(xhr.responseText)
            var err = eval("(" + xhr.responseText + ")");
            throw err;
        }
    });
    //
});

function createSimpleFilteringGrid() {
    $("#gridSimpleFiltering").igGrid({
        autoGenerateColumns: false,
        columns: [
            { headerText: "RackBinIdAse", key: "RackBinIdAse", dataType: "string", hidden: true },
            { headerText: "Rack Description", key: "RackDescription", dataType: "string" },
            { headerText: "Bin Description", key: "BinDescription", dataType: "string" },
            { headerText: "Action", key: "Action", template: "<a href='" + Config.url + "Administration/StockArea/BinEdit/${RackBinIdAse}' class='dwn_btnt'><img title='Edit' src='" + Config.url + "Client/img/Edit.png'></a> <a href='" + Config.url + "Administration/StockArea/BinDelete/${RackBinIdAse}' class='dwn_btnt'><img title='Delete' src='" + Config.url + "Client/img/delete.png'></a>", width: "12%", filter: "disable" }

        ],
        dataSource: data,
        renderCheckboxes: true,
        responseDataKey: "results",
        features: [
            {
                name: "Filtering",
                type: "local",
                mode: "simple",
                filterDialogContainment: "window",
                columnSettings: [
                {
                    columnKey: "PurchaseOrderNumber",
                    condition: "startsWith"

                },
                { columnKey: "Action", allowFiltering: false }
                ]

            },
            {
                name: "Paging",
                type: "local",
                pageSize: 10
            }
        ]
    });
    $("#Ips_loader_div").hide();
}