﻿$("document").ready(function () {
    //remove  the validation
    $("#VendorName").keyup(function () {
        $("#PartNumberUnique").hide();
        $("#part_btn").attr("disabled", false);
    });

    $("#VendorName").blur(function () {
        var _Name = $("#VendorName").val();       
        if (_Name != "" && _Name != null && _Name != undefined) {
            $("#PartNumberUnique").hide();
            $("#Ips_loader_div").show();
            setTimeout(function () { getVendorName(); }, 2000);
        }
    });
});



function getVendorName() {
    var turl = Config.url + 'Administration/Vendor/Jsonname';
    var _Name = $("#VendorName").val();
    if (_Name != "" && _Name != null && _Name != undefined) {
        console.log(_Name);
        $.ajax({
            type: "GET",
            url: turl,
            data: { Vedorname: _Name },
            async: false,
            dataType: 'json',
            success: function (data) {
                if (data == true) {
                    $("#PartNumberUnique").show();
                    $("#part_btn").attr("disabled", true);
                }
            }
        });
    }
    $("#Ips_loader_div").hide();
}