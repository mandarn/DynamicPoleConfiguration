﻿/// <reference path="AddVendor.js" />
/// <reference path="RackList.js" />
$(function () {

    //$('#VendorId').select2();

    //////////Auto complete box

    var getData = function (request, response) {
        var _name = $("#VendorId").val();
        var _RecieptID = $("#ReceiptType").val();
        $.getJSON(
            Config.url + 'Transaction/Receipt/AutocompleteNumber?number=' + request.term + '&name=' + _name + "&ReceiptType=" + _RecieptID,
            function (data) {
                response(data);
            });
    };

    
    $(".autocomplete_number").autocomplete({
        source: getData
    });


   //// --------------------------------Purchase Order number check------------------------


     var getData = function (request, response) {
         var _ordernumber = $("#PurchaseOrderNumber").val();
        $.getJSON(
            Config.url + 'Transaction/Receipt/AutocompletePartNumber?number=' + request.term + '&ordernumber=' + _ordernumber,
            function (data) {
                response(data);
            });
     };


     $(".autocomplete_part").autocomplete({
         source: getData
     });


    //////-------------------------Stock area auto complete-----------------------------


     var getData = function (request, response) {        
         $.getJSON(
             Config.url + 'Transaction/Receipt/StockDescription?text=' + request.term,
             function (data) {
                 console.log(data);
                 response($.map(data, function (item) {
                     return {
                         label: item.StockAreaDescription,
                         val: item.StockAreaId
                     }
                 }));
             });
     };

     $("#StockAreaDescription").autocomplete({
         source: getData,
         select: function (e, i) {
             $("#StockAreaID").val(i.item.val);
         }
     });



    ////-----------------------------Transfer Part Number-------------------------------



     var getData = function (request, response) {
         var _StockId = $("#Fromstockarea").val();
         $.getJSON(
             Config.url + 'Transaction/Transfer/PartNumberAuto?Partnumber=' + request.term + '&StockId=' + _StockId,
             function (data) {
                 console.log(data);
                 response($.map(data, function (item) {
                     return {
                         label: item,
                         val: item
                     }
                 }));
             });
     };

     $(".transfer_number").autocomplete({
         source: getData,
         select: function (e, i) {
            /// getTransferCalculation();
         }
     });




});