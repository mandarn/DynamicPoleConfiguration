﻿$(function () {
    var getPartData = function (request, response) {
        var _StockAreaID = $("#StockAreaID").val();
        if (_StockAreaID != "") {
            $.getJSON(
                Config.url + 'Transaction/Consume/AutocompletePartNumber?number=' + request.term + '&StockAreaID=' + _StockAreaID,
                function (data) {
                    response(data);
                });
        }
        else {
            $("#stock_error").show();
        }
    };

    $("#PartNumber").autocomplete({
        source: getPartData
    });



});

//
$(document).ready(function () {
    List();
});
function List() {
    $.ajax({
        type: "Get",
        url: Config.url + 'Transaction/Consume/JsonList',
        dataType: 'json',
        accept: 'application/json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            data = response;
            createSimpleFilteringGrid();
        },
        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            throw err;
        }
    });
}

function FilteredList() {
    var StartDate = null;
    var EndDate = null;
    if ($("#txtDates").val() != "") {
        StartDate = new Date($("#txtDates").val().split("-")[0]).toISOString()
        EndDate = new Date($("#txtDates").val().split("-")[1]).toISOString()
    }

    $.ajax({
        type: "Get",
        url: Config.url + 'Transaction/Consume/FilteredJsonList',
        dataType: 'json',
        accept: 'application/json',
        contentType: 'application/json; charset=utf-8',
        data: {
            PartNumber: $("#PartNumber").val(),
            StartDate: StartDate,
            EndDate: EndDate
        },
        success: function (response) {
            data = response;
            createSimpleFilteringGrid();
        },
        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            throw err;
        }
    });
}



function createSimpleFilteringGrid() {
    $("#gridSimpleFiltering").igGrid({
        autoGenerateColumns: false,
        columns: [
            {
                headerText: "CheckInCheckOutID", key: "CheckInCheckOutID", dataType: "int", hidden: true
            },
             {
                 headerText: "CheckInCheckOutIDAse", key: "CheckInCheckOutIDAse", dataType: "int", hidden: true
             },

            {
                headerText: "Part Number", key: "PartNumber", dataType: "string"
            },
            {
                headerText: "Date", key: "Date", dataType: "date"
            },

            //{
            //    headerText: "Stock Area", key: "StockArea", dataType: "string"
            //},
            //{
            //    headerText: "Part Number", key: "PartNumber", dataType: "string"
            //},
            //{
            //    headerText: "Quantity", key: "Quantity", dataType: "int"
            //},

                  {
                      headerText: "Action", key: "Action", template: "<a href='" + Config.url + "Transaction/Consume/Add/${CheckInCheckOutIDAse}'><img title='Add RMAOrderPart' class='button_add_prt tab_sidebar' src='" + Config.url + "Client/img/add.png'/></a> <a href='" + Config.url + "Transaction/Consume/Add/${CheckInCheckOutIDAse}' class='dwn_btnt'><img title='Edit RMA' src='" + Config.url + "Client/img/Edit.png'></a> <a href='" + Config.url + "Transaction/Consume/Delete/${CheckInCheckOutIDAse}' class='dwn_btnt'><img title='Delete RMA' src='" + Config.url + "Client/img/delete.png'></a>", width: "12%"
                  }

        ],
        dataSource: data,
        renderCheckboxes: true,
        responseDataKey: "results",
        features: [
{
    name: "Filtering",
    type: "local",
    mode: "simple",
    filterDialogContainment: "window",
    columnSettings: [
        {
            columnKey: "PurchaseOrderNumber",
            condition: "startsWith"

        },
{
    columnKey: "Action", allowFiltering: false
}
    ]

},
{
    name: "Paging",
    type: "local",
    pageSize: 10
}
        ]
    });
    $("#Ips_loader_div").hide();
}
var _PartNumber = $("#PartNumber");

_PartNumber.keyup(function () {
    //$("#partnumber_error").hide();
    //$("#part_btn").attr("disabled", false);
    //$(".input_disable").attr("disabled", false);
    //$("#stock_error").hide();
});

//_PartNumber.blur(function () {
//    $("#partnumber_error").hide();
//    var PartNumber = _PartNumber.val();
//    if (PartNumber != "" && PartNumber != null && PartNumber != undefined) {
//        $("#Ips_loader_div").show();
//        setTimeout(function () { getPartnumber(); }, 500);
//    }
//});


function RowAdd() {
    var AppendString = "";
    AppendString += "<tr role='row' tabindex='0'>" +
        "<td role='gridcell' aria-readonly='false' aria-describedby='gridSimpleFiltering_CheckType' tabindex='0' class=''>" + $("#type option:selected").text() + "</td>" +
    "<td role='gridcell' aria-readonly='false' aria-describedby='gridSimpleFiltering_StockArea' tabindex='0' class=''>" + $("#StockAreaID option:selected").text() + "</td>" +
    "<td role='gridcell' aria-readonly='false' aria-describedby='gridSimpleFiltering_PartNumber' tabindex='0' class=''>" + $("#PartNumber").val() + "</td>" +
    "<td role='gridcell' aria-readonly='false' aria-describedby='gridSimpleFiltering_Quantity' tabindex='0' class=''>" + $("#Quantity").val() + "</td>" +
    "<td role='gridcell' aria-readonly='false' aria-describedby='gridSimpleFiltering_Action' tabindex='0' class=''>" +
    "<a href='http://localhost:61092/Transaction/Consume/RMAOrderPartAdd/'>" +
    "<img title='Add RMAOrderPart' class='button_add_prt tab_sidebar' src='http://localhost:61092/Client/img/add.png'></a>" +
    "<a href='http://localhost:61092/Transaction/Consume/Edit/' class='dwn_btnt'>" +
    "<img title='Edit RMA' src='http://localhost:61092/Client/img/Edit.png'></a>" +
    "<a href='http://localhost:61092/Transaction/Consume/Delete/' class='dwn_btnt'>" +
    "<img title='Delete RMA' src='http://localhost:61092/Client/img/delete.png'></a></td>" +
    "</tr>";

    $(".ui-iggrid-tablebody").append(AppendString);
}

