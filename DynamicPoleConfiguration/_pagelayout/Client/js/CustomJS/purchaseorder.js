﻿$(function () {
    $("#Ips_loader_div").show();
    setTimeout(function () { List(); }, 500);
});

//
function List() {
    $.ajax({
        type: "Get",
        url: Config.url + 'Transaction/Purchase/JsonList',
        dataType: 'json',
        accept: 'application/json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            data = response;
            createSimpleFilteringGrid();
        },
        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            throw err;
        }
    });
}


function FilteredList() {
    var StartDate = null;
    var EndDate = null;
    if ($("#txtDates").val() != "") {
        StartDate = new Date($("#txtDates").val().split("-")[0]).toISOString()
        EndDate = new Date($("#txtDates").val().split("-")[1]).toISOString()
    }

    $.ajax({
        type: "Get",
        url: Config.url + 'Transaction/Purchase/FilteredJsonList',
        dataType: 'json',
        accept: 'application/json',
        contentType: 'application/json; charset=utf-8',
        data: {
            status: $("#Status").val(),
            PurchaseOrderNumber: $("#PON").val(),
            StartDate: StartDate,
            EndDate: EndDate
        },
        success: function (response) {
            data = response;
            createSimpleFilteringGrid();
        },
        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            throw err;
        }
    });
}

function createSimpleFilteringGrid() {
    $("#gridSimpleFiltering").igGrid({
        autoGenerateColumns: false,
        columns: [
            { headerText: "PurchaseOrderID", key: "PurchaseOrderID", dataType: "int", hidden: true },
             { headerText: "PurchaseOrderIDASe", key: "PurchaseOrderIDASe", dataType: "int", hidden: true },
            { headerText: "Order Number", key: "PurchaseOrderNumber", dataType: "string", width: "12%" },
            { headerText: "Date", key: "Date", dataType: "string", width: "12%" },
            { headerText: "Vendor", key: "VendorName", dataType: "string", width: "12%" },
            { headerText: "Status", key: "OrderStatus", dataType: "string", width: "12%" },
            { headerText: "Action", key: "Action", template: "<a href='" + Config.url + "Transaction/Purchase/PurchaseOrderPartAdd/${PurchaseOrderIDASe}'><img title='Add PurchaseOrderPart' class='button_add_prt tab_sidebar' src='" + Config.url + "Client/img/add.png'/></a> <a href='" + Config.url + "Transaction/Purchase/Edit/${PurchaseOrderIDASe}' class='dwn_btnt'><img title='Edit Purchase' src='" + Config.url + "Client/img/Edit.png'></a> <a href='" + Config.url + "Transaction/Purchase/Delete/${PurchaseOrderIDASe}' class='dwn_btnt'><img title='Delete Purchase' src='" + Config.url + "Client/img/delete.png'></a>", width: "12%" }

        ],
        autofitLastColumn: false,
        dataSource: data,
        dataSourceType: "json",
        width: "100%",
      features: [
            {
                name: "Filtering",
                type: "local",
                mode: "simple",
                filterDialogContainment: "window",
                columnSettings: [
                {
                    columnKey: "PurchaseOrderNumber",
                    condition: "startsWith"

                },
                { columnKey: "Action", allowFiltering: false }
                ]

            },
            {
                name: "Paging",
                type: "local",
                pageSize: 10
            }
        ]

    });
    $("#Ips_loader_div").hide();
}





