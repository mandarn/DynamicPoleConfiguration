﻿$(function () {
    $("#Ips_loader_div").show();
    setTimeout(function () { List(); }, 1000);
});


function List() {
    $.ajax({
        type: "Get",
        url: Config.url + 'Administration/StockArea/JsonList',
        dataType: 'json',
        accept: 'application/json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            data = response;
            createSimpleFilteringGrid();
        },
        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            throw err;
        }
    });
    $("#Ips_loader_div").hide();
}

function createSimpleFilteringGrid() {
    $("#gridSimpleFiltering").igGrid({
        autoGenerateColumns: false,
        columns: [
            { headerText: "StockArea Id", key: "StockAreaId", dataType: "int", hidden: true },
            { headerText: "StockAreaIdAse", key: "StockAreaIdAse", dataType: "string", hidden: true },
            { headerText: "Description", key: "StockAreaDescription", dataType: "string" ,Align:"left"},
            { headerText: "Action", key: "Action", template: "<a href='" + Config.url + "Administration/StockArea/RackAdd/${StockAreaIdAse}' class='dwn_btnt'><img  class='button_edit_r tab_sidebar' title='Add Rack' src='" + Config.url + "Client/img/add.png'> </a> <a href='" + Config.url + "Administration/StockArea/Edit/${StockAreaIdAse}' class='dwn_btnt'><img  class='button_edit_r tab_sidebar' title='Edit Stock Area' src='" + Config.url + "Client/img/Edit.png'></a> <a href='" + Config.url + "Administration/StockArea/Delete/${StockAreaIdAse}' class='dwn_btnt'><img title='Delete Stock Area' src='" + Config.url + "Client/img/delete.png'></a>", width: "15%" },
            ],
        dataSource: data,
        renderCheckboxes: true,
        responseDataKey: "results",
        features: [
            {
                name: "Filtering",
                type: "local",
                mode: "simple",
                filterDialogContainment: "window",
                columnSettings: [
                {
                    columnKey: "StockAreaDescription",
                    condition: "startsWith"

                },
                 { columnKey: "Action", allowFiltering: false }
                ]

            },
            {
                name: "Paging",
                type: "local",
                pageSize: 10
            }
        ]
    });
}