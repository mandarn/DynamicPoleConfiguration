﻿$(function () {
    $("#Ips_loader_div").show();
    setTimeout(function () { List(); }, 500);

});



function List() {
    $.ajax({
        type: "Get",
        url: Config.url + 'Transaction/Transfer/JsonList',
        dataType: 'json',
        accept: 'application/json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            data = response;
            createSimpleFilteringGrid();
        },
        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            throw err;
        }
    });
}
function FilteredList() {
    var StartDate = null;
    var EndDate = null;
    if ($("#txtDates").val() != "") {
        StartDate = new Date($("#txtDates").val().split("-")[0]).toISOString()
        EndDate = new Date($("#txtDates").val().split("-")[1]).toISOString()
    }

    $.ajax({
        type: "Get",
        url: Config.url + 'Transaction/Transfer/FilteredJsonList',
        dataType: 'json',
        accept: 'application/json',
        contentType: 'application/json; charset=utf-8',
        data: {
            StockareaID: $("#Fromstockarea").val(),
            PartNumber: $("#PartNumber").val(),
            StartDate: StartDate,
            EndDate: EndDate
        },
        success: function (response) {
            data = response;
            createSimpleFilteringGrid();
        },
        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            throw err;
        }
    });
}

///// 

function createSimpleFilteringGrid() {
    $("#gridSimpleFiltering").igGrid({
        autoGenerateColumns: false,
        columns: [
            { headerText: "PartReceiptDetailIDAse", key: "PartReceiptDetailIDAse", dataType: "string", hidden: true },
            { headerText: "Part Number", key: "PartNumber", dataType: "string" },
            { headerText: "From Stock Area Description", key: "FromStockAreaDescription", dataType: "string" },
            { headerText: "To Stock Area Description", key: "ToStockAreaDescription", dataType: "string" },                       
            { headerText: "Quantity", key: "ToQuantity", dataType: "string" },
            { headerText: "Transfer Date", key: "ReceiptDate", dataType: "date" },
            { headerText: "Action", key: "Action", template: "<a href='" + Config.url + "Transaction/Transfer/Edit/${PartReceiptDetailIDAse}' class='dwn_btnt'><img title='Edit' src='" + Config.url + "Client/img/Edit.png'></a> <a href='" + Config.url + "Transaction/Transfer/Delete/${PartReceiptDetailIDAse}' class='dwn_btnt'><img title='Delete' src='" + Config.url + "Client/img/delete.png'></a>", width: "12%" }

        ],
        dataSource: data,
        renderCheckboxes: true,
        responseDataKey: "results",
        features: [
            {
                name: "Filtering",
                type: "local",
                mode: "simple",
                filterDialogContainment: "window",
                columnSettings: [
                {
                    columnKey: "PurchaseOrderNumber",
                    condition: "startsWith"

                },
                 { columnKey: "Action", allowFiltering: false }
                ]

            },
            {
                name: "Paging",
                type: "local",
                pageSize: 10
            }
        ]
    });
    $("#Ips_loader_div").hide();
}