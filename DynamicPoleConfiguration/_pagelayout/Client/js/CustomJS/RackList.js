﻿$(document).ready(function () {
    $("#Ips_loader_div").show();
    var Id = $("#StockAreaIdAse").val();
    $.ajax({
        type: "Get",
        url: Config.url + 'Administration/StockArea/RackJsonList',
        data: { StockId: Id },
        dataType: 'json',
        accept: 'application/json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            //console.log(response);
            data = response;
            setTimeout(function () { createSimpleFilteringGrid(); }, 500);
        },
        error: function (xhr, status, error) {
            $("#Ips_loader_div").hide();
            //console.log(xhr.responseText)
            var err = eval("(" + xhr.responseText + ")");
            throw err;
        }
    });
    //
});

function createSimpleFilteringGrid() {
    $("#gridSimpleFiltering").igGrid({
        autoGenerateColumns: false,
        columns: [
            { headerText: "StockRackIdAse", key: "StockRackIdAse", dataType: "string", hidden: true },
            { headerText: "Rack Description", key: "RackDescription", dataType: "string" },
            { headerText: "Stock Area Description", key: "StockAreaDescription", dataType: "string" },
            { headerText: "Action", key: "Action", template: "<a href='" + Config.url + "Administration/StockArea/BinAdd/${StockRackIdAse}' class='dwn_btnt'><img title='Add Bin' src='" + Config.url + "Client/img/add.png'></a> <a href='" + Config.url + "Administration/StockArea/RackEdit/${StockRackIdAse}' class='dwn_btnt'><img title='Edit Rack' src='" + Config.url + "Client/img/Edit.png'></a> <a href='" + Config.url + "Administration/StockArea/RackDelete/${StockRackIdAse}' class='dwn_btnt'><img title='Delete Rack' src='" + Config.url + "Client/img/delete.png'></a>", width: "12%", filter: "disable" }

        ],
        dataSource: data,
        renderCheckboxes: true,
        responseDataKey: "results",
        features: [
            {
                name: "Filtering",
                type: "local",
                mode: "simple",
                filterDialogContainment: "window",
                columnSettings: [
                {
                    columnKey: "PurchaseOrderNumber",
                    condition: "startsWith"

                },
                { columnKey: "Action", allowFiltering: false }
                ]

            },
            {
                name: "Paging",
                type: "local",
                pageSize: 10
            }
        ]
    });
    $("#Ips_loader_div").hide();
}