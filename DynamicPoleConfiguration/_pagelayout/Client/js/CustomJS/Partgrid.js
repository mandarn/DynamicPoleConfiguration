﻿/// <reference path="LayOutEvents.js" />
$(function () {
    $("#Ips_loader_div").show();
    setTimeout(function () { List(); }, 500);
});


function List() {
    $.ajax({
        type: "Get",
        url: Config.url + 'Administration/Part/JsonList',
        dataType: 'json',
        accept: 'application/json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            data = response;
            createSimpleFilteringGrid();
        },
        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            throw err;
        }
    });
    $("#Ips_loader_div").hide();
}

function createSimpleFilteringGrid() {
    $("#gridSimpleFiltering").igGrid({
        autoGenerateColumns: false,
        columns: [
            { headerText: "Part Id", key: "PartId", dataType: "number", hidden: true },
            { headerText: "PartEncry", key: "PartIdAse", dataType: "string", hidden: true },
            { headerText: "Part Number", key: "PartNumber", dataType: "string" },
            { headerText: "Description", key: "Description", dataType: "string" },
            { headerText: "Lead Time", key: "LeadTime", dataType: "number" },
            { headerText: "Rate", key: "Rate", dataType: "number" },
            { headerText: "Action", key: "Action", template: "<a href='" + Config.url + "Administration/Part/Edit/${PartIdAse}', class='dwn_btnt'><img title='Edit Part' src='" + Config.url + "Client/img/Edit.png'></a> <a href='" + Config.url + "Administration/Part/Delete/${PartIdAse}',class='dwn_btnt conform_delete'><img title='Delete Part' src='" + Config.url + "Client/img/Delete.png'></a>", width: "12%" }

        ],
        dataSource: data,
        renderCheckboxes: true,
        responseDataKey: "results",
        features: [
            {
                name: "Filtering",
                type: "local",
                mode: "simple",
                filterDialogContainment: "window",
                columnSettings: [
                {
                    columnKey: "PartNumber",
                    condition: "startsWith"

                },
                  { columnKey: "Action", allowFiltering: false }
                ]

            },
            {
                name: "Paging",
                type: "local",
                pageSize: 10
            }
        ]
    });
}