﻿$(document).ready(function () {
    $("#Ips_loader_div").show();
    var PurchaseOrerId = $("#PurchaseOrderId").val();
    var _PartNumber = $("#PartNumber");
    var _Rate = $("#Rate");
    var _QuantityRequested = $("#QuantityRequested");
    $.ajax({
        type: "Get",
        url: Config.url + 'Transaction/Purchase/JsonPurchaseOrderParList',
        dataType: 'json',
        data: { "PurchaseOrderId": PurchaseOrerId },
        accept: 'application/json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            //console.log(response);
            data = response;
            setTimeout(function () { createSimpleFilteringGrid(); }, 500);
        },
        error: function (xhr, status, error) {
            $("#Ips_loader_div").hide();
            //console.log(xhr.responseText)
            var err = eval("(" + xhr.responseText + ")");
            throw err;
        }
    });

    //////////Auto complete box

    var getData = function (request, response) {
        $.getJSON(
            Config.url + 'Transaction/Purchase/AutocompleterPartNumber?number=' + request.term,
            function (data) {
                response(data);
            });
    };

    //////////////////////////////////////////////////////////////

    $(".autocomplete").autocomplete({
        source: getData
    });

    _PartNumber.keyup(function () {
        $("#PartNumberUnique").hide();
        $("#part_btn").attr("disabled", false);
    });

    _PartNumber.blur(function () {
        var getCount = $(".ui-autocomplete li").length;
        if (getCount == 1) {
            $("#PartNumber").val($(".ui-autocomplete li").html());
        }
        else {
            var _PurchaseOrderNumber = $("#PurchaseOrderNumber").val();
            var _Number = $("#PartNumber").val();
            if ((_Number != "")) {
                $("#PartNumberUnique").hide();
                $("#Ips_loader_div").show();
                setTimeout(function () { getPartNumber(); }, 1000);
            }
        }
    });

});

//////////////////////////////////////////////////////////////////////////

function getPartNumber() {
    var turl = Config.url + 'Transaction/Purchase/Jsonpart';
    var _PurchaseOrderNumber = $("#PurchaseOrderNumber").val();
    var _Number = $("#PartNumber").val();
    if (_Number != "" && _Number != null && _Number != undefined) {
        $.ajax({
            type: "GET",
            url: turl,
            data: { partNumber: _Number, purchaseNumber: _PurchaseOrderNumber },
            async: false,
            dataType: 'json',
            success: function (data) {
                if (data.NumberCheck == true) {
                    $("#PartNumberUnique").show();
                    $("#part_btn").attr("disabled", true);
                } else if (data.NumberStatus == true) {
                    $("#PartNumberUnique").show();
                    $("#part_btn").attr("disabled", true);
                }
                else {
                    $("#Rate").val(data.PartRate)
                }
            }
        });
    }
    $("#Ips_loader_div").hide();
}

/////////////////////////////////////////////////////////////////////////////

function createSimpleFilteringGrid() {
    $("#gridSimpleFiltering").igGrid({
        autoGenerateColumns: false,
        columns: [
            { headerText: "PurchaseOrderPartId", key: "PurchaseOrderPartId", dataType: "int", hidden: true },
             { headerText: "PurchaseOrderPartIdAse", key: "PurchaseOrderPartIdAse", dataType: "int", hidden: true },
            { headerText: "Purchase Order Number", key: "PurchaseOrderNumber", dataType: "int" },
            { headerText: "Part Number", key: "PartNumber", dataType: "int" },
            { headerText: "Quantity", key: "QuantityRequested", dataType: "int" },
            { headerText: "Rate", key: "Rate", dataType: "int" },
            { headerText: "Action", key: "Action", template: "<a href='" + Config.url + "Transaction/Purchase/PurchaseOrderPartEdit/${PurchaseOrderPartIdAse}' class='dwn_btnt'><img title='Edit PurchaseOrderPart' src='" + Config.url + "Client/img/Edit.png'></a> <a href='" + Config.url + "Transaction/Purchase/PurchaseOrderPartDelete/${PurchaseOrderPartIdAse}' class='dwn_btnt'><img title='Delete PurchaseOrderPart' src='" + Config.url + "Client/img/delete.png'></a>", width: "12%", filter: "disable" }

        ],
        dataSource: data,
        renderCheckboxes: true,
        responseDataKey: "results",
        features: [
            {
                name: "Filtering",
                type: "local",
                mode: "simple",
                filterDialogContainment: "window",
                columnSettings: [
                {
                    columnKey: "PurchaseOrderNumber",
                    condition: "startsWith"

                },
                { columnKey: "Action", allowFiltering: false }
                ]

            },
            {
                name: "Paging",
                type: "local",
                pageSize: 10
            }
        ]
    });
    $("#Ips_loader_div").hide();
}

//////////////////////////////////////////////////////////////////////////////

