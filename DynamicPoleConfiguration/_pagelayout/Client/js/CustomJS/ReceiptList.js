﻿$(function () {
    $("#Ips_loader_div").show();
    setTimeout(function () { List(); }, 500);
});


function List() {
    $.ajax({
        type: "Get",
        url: Config.url + 'Transaction/Receipt/JsonList',
        dataType: 'json',
        accept: 'application/json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            data = response;
            createSimpleFilteringGrid();
        },
        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            throw err;
        }
    });
    $("#Ips_loader_div").hide();
}




function FilteredList() {
    var StartDate = null;
    var EndDate = null;
    if ($("#txtDates").val() != "") {
        StartDate = new Date($("#txtDates").val().split("-")[0]).toISOString()
        EndDate = new Date($("#txtDates").val().split("-")[1]).toISOString()
    }

    $.ajax({
        type: "Get",
        url: Config.url + 'Transaction/Receipt/FilteredJsonList',
        dataType: 'json',
        accept: 'application/json',
        contentType: 'application/json; charset=utf-8',
        data: {
            ReceiptType: $("#ReceiptType").val(),
            VendorId: $("#VendorId").val(),
            OrderNumber: $("#PurchaseOrderNumber").val(),
            StartDate: StartDate,
            EndDate: EndDate
        },
        success: function (response) {
            data = response;
            createSimpleFilteringGrid();
        },
        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            throw err;
        }
    });
}

function createSimpleFilteringGrid() {
    $("#gridSimpleFiltering").igGrid({
        autoGenerateColumns: false,
        columns: [
            { headerText: "PartReceiptIDAse", key: "PartReceiptIDAse", dataType: "string", hidden: true },
            { headerText: "Receipt Type", key: "ReceiptType", dataType: "string" },
            { headerText: "Reference Number", key: "PurchaseOrderNumber", dataType: "string" },
            { headerText: "Vendor Name", key: "VendorName", dataType: "string" },
            { headerText: "Receipt Date", key: "ReceiptDate", dataType: "date" },                       
            { headerText: "Action", key: "Action", template: "<a href='" + Config.url + "Transaction/Receipt/DetailAdd/${PartReceiptIDAse}' class='dwn_btnt'><img  class='button_edit_r tab_sidebar' title='Add Receipt Detail' src='" + Config.url + "Client/img/add.png'> </a> <a href='" + Config.url + "Transaction/Receipt/Edit/${PartReceiptIDAse}' class='dwn_btnt'><img title='Edit Receipt' src='" + Config.url + "Client/img/Edit.png'></a> <a href='" + Config.url + "Transaction/Receipt/Delete/${PartReceiptIDAse}' class='dwn_btnt'><img title='Delete Receipt' src='" + Config.url + "Client/img/delete.png'></a>", width: "15%" },
        ],
        dataSource: data,
        renderCheckboxes: true,
        responseDataKey: "results",
        features: [
            {
                name: "Filtering",
                type: "local",
                mode: "simple",
                filterDialogContainment: "window",
                columnSettings: [
                {
                    columnKey: "StockAreaDescription",
                    condition: "startsWith"

                },
                 { columnKey: "Action", allowFiltering: false }
                ]

            },
            {
                name: "Paging",
                type: "local",
                pageSize: 10
            }
        ]
    });
}



