﻿$(function () {
    /////purchase order form      

    $("#PurchaseOrderNumber").blur(function () {     
        var _PurchaseOrderNumber = $("#PurchaseOrderNumber").val();
        if (_PurchaseOrderNumber != "") {
            $("#Ips_loader_div").show();
            setTimeout(function () { getPurchaseNumber(); }, 1000);
        }       
    });


    $("#PurchaseOrderNumber").keyup(function () {
        $("#purchaseordernumber").hide();
        $("#part_btn").attr("disabled", false);
    });  

});


/////check purchase number
function getPurchaseNumber() {
    debugger
    var turl = Config.url + 'Transaction/Purchase/CheckNumber';
    var _PurchaseOrderNumber = $("#PurchaseOrderNumber").val();
    if (_PurchaseOrderNumber != "" && _PurchaseOrderNumber != null && _PurchaseOrderNumber != undefined) {
        $.ajax({
            type: "GET",
            url: turl,
            data: { number: _PurchaseOrderNumber },
            async: false,
            dataType: 'json',
            success: function (data) {
                if (data == true) {
                    $("#purchaseordernumber").show();                   
                    $("#part_btn").attr("disabled", true);
                } else {
                    $("#purchaseordernumber").hide();
                    $("#part_btn").attr("disabled", false);
                }
            }
        });
    }
    $("#Ips_loader_div").hide();
}
