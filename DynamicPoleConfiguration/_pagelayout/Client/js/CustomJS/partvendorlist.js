﻿$(document).ready(function () {
    $("#Ips_loader_div").show();
    var Id = $("#VendorID").val();
    $.ajax({
        type: "Get",
        url: Config.url + 'Administration/PartVendor/JsonList',
        data: { Id: Id },
        dataType: 'json',
        accept: 'application/json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {           
            //console.log(response);
            data = response;
            setTimeout(function () { createSimpleFilteringGrid(); }, 500);
        },
        error: function (xhr, status, error) {
            $("#Ips_loader_div").hide();
            //console.log(xhr.responseText)
            var err = eval("(" + xhr.responseText + ")");
            throw err;
        }
    });
    //
});

function createSimpleFilteringGrid() {
    $("#gridSimpleFiltering").igGrid({
        autoGenerateColumns: false,
        columns: [
            { headerText: "PartVendorMappingID", key: "PartVendorMappingID", dataType: "int", hidden: true },
             { headerText: "PartVendorMappingIDAse", key: "PartVendorMappingIDAse", dataType: "int", hidden: true },
            { headerText: "Part Number", key: "PartNumber", dataType: "string" },
            { headerText: "Description", key: "Description", dataType: "string" },
            { headerText: "Rate", key: "Rate", dataType: "string" },
            { headerText: "Lead Time", key: "LeadTime", dataType: "string" },
            { headerText: "Action", key: "Action", template: "<a href='" + Config.url + "Administration/PartVendor/Edit/${PartVendorMappingIDAse}' class='dwn_btnt'><img title='Edit PartVendor' src='" + Config.url + "Client/img/Edit.png'></a> <a href='" + Config.url + "Administration/PartVendor/Delete/${PartVendorMappingIDAse}' class='dwn_btnt'><img title='Delete PartVendor' src='" + Config.url + "Client/img/delete.png'></a>", width: "12%", filter: "disable" }

        ],
        dataSource: data,
        renderCheckboxes: true,
        responseDataKey: "results",
        features: [
            {
                name: "Filtering",
                type: "local",
                mode: "simple",
                filterDialogContainment: "window",
                columnSettings: [
                {
                    columnKey: "PurchaseOrderNumber",
                    condition: "startsWith"

                },
                { columnKey: "Action", allowFiltering: false }
                ]

            },
            {
                name: "Paging",
                type: "local",
                pageSize: 10
            }
        ]
    });
    $("#Ips_loader_div").hide();
}