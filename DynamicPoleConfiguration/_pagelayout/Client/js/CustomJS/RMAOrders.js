﻿$(function () {
    $("#Ips_loader_div").show();
    setTimeout(function () { List(); }, 500);
});

//
function List() {
    $.ajax({
        type: "Get",
        url: Config.url + 'Transaction/RMA/JsonList',
        dataType: 'json',
        accept: 'application/json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            data = response;
            createSimpleFilteringGrid();
        },
        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            throw err;
        }
    });
}

function FilteredList() {
    var StartDate = null;
    var EndDate = null;
    if ($("#txtDates").val() != "") {
        StartDate = new Date($("#txtDates").val().split("-")[0]).toISOString()
        EndDate = new Date($("#txtDates").val().split("-")[1]).toISOString()
    }

    $.ajax({
        type: "Get",
        url: Config.url + 'Transaction/RMA/FilteredJsonList',
        dataType: 'json',
        accept: 'application/json',
        contentType: 'application/json; charset=utf-8',
        data: {
            VendorId: $("#VendorId").val(),
            RMANumber: $("#RON").val(),
            StartDate: StartDate,
            EndDate: EndDate
        },

        success: function (response) {
            data = response;
            createSimpleFilteringGrid();
        },
        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            throw err;
        }
    });
}

function createSimpleFilteringGrid() {
    $("#gridSimpleFiltering").igGrid({
        autoGenerateColumns: false,
        columns: [
            { headerText: "PartReceiptID", key: "PartReceiptID", dataType: "int", hidden: true },
             { headerText: "RMAOrderIDAse", key: "RMAOrderIDAse", dataType: "string", hidden: true },
            { headerText: "Order Number", key: "RMANumber", dataType: "string" },
            { headerText: "Date", key: "Date", dataType: "string" },
            { headerText: "Vendor", key: "VendorName", dataType: "string" },
            { headerText: "Action", key: "Action", template: "<a href='" + Config.url + "Transaction/RMA/RMAOrderPartAdd/${RMAOrderIDAse}'><img title='Add RMAOrderPart' class='button_add_prt tab_sidebar' src='" + Config.url + "Client/img/add.png'/></a> <a href='" + Config.url + "Transaction/RMA/Edit/${RMAOrderIDAse}' class='dwn_btnt'><img title='Edit RMA' src='" + Config.url + "Client/img/Edit.png'></a> <a href='" + Config.url + "Transaction/RMA/Delete/${RMAOrderIDAse}' class='dwn_btnt'><img title='Delete RMA' src='" + Config.url + "Client/img/delete.png'></a>", width: "12%" }

        ],
        dataSource: data,
        renderCheckboxes: true,
        responseDataKey: "results",
        features: [
            {
                name: "Filtering",
                type: "local",
                mode: "simple",
                filterDialogContainment: "window",
                columnSettings: [
                {
                    columnKey: "PurchaseOrderNumber",
                    condition: "startsWith"

                },
                 { columnKey: "Action", allowFiltering: false }
                ]

            },
            {
                name: "Paging",
                type: "local",
                pageSize: 10
            }
        ]
    });
    $("#Ips_loader_div").hide();
}