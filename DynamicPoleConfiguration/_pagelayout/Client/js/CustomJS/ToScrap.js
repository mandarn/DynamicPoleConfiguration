﻿$(function () {

    var _Fromstockarea = $("#Fromstockarea");
    var _PartNumber = $("#PartNumber");
    var _ToQuantity = $("#ToQuantity");
    var _FromQuantity = $("#FromQuantity");

    _Fromstockarea.change(function () {
        _PartNumber.val('');
        _ToQuantity.val('');
        _FromQuantity.val('');
        $("#Fromstockarea_error").hide();
    });

    _PartNumber.keyup(function () {
        _ToQuantity.val('');
        _FromQuantity.val('');    
        var Fromstockarea = _Fromstockarea.val();       
        if (Fromstockarea != "" && Fromstockarea != null && Fromstockarea != undefined) {
            $("#part_btn").attr("disabled", false);           
        } else {
            $("#part_btn").attr("disabled", true);
            $("#Fromstockarea_error").show();
        }        
    });

    _PartNumber.blur(function () {
        var getCount = $(".ui-autocomplete li").length;
        if (getCount == 1) {
            $("#PartNumber").val($(".ui-autocomplete li").html());
        }
        else {
            var Fromstockarea = _Fromstockarea.val();
            if (Fromstockarea != "" && Fromstockarea != null && Fromstockarea != undefined) {
                $("#Ips_loader_div").show();
                setTimeout(function () { getTransferCalculation(); }, 2000);
            }
        }
    });

    //QunatityGet validation
    _ToQuantity.keyup(function () {
        var FromQuantity = _FromQuantity.val();
        var ToQuantity = _ToQuantity.val();
        var _status = (Number(ToQuantity) <= Number(FromQuantity));
        var _zero = (Number(ToQuantity) > Number(0));
        if (_zero == true) {
             if (_status == true) {
                 _ToQuantity.val(ToQuantity);
                } else {
                 _ToQuantity.val('');
                }
         } else {
            _ToQuantity.val('');
        }
    });

});


function getTransferCalculation() {

    var turl = Config.url + 'Transaction/Scrap/ScrapCaluclation';
    var _StockId = $("#Fromstockarea").val();
    var _PartNumber = $("#PartNumber").val();

    if (_PartNumber != "" && _PartNumber != null && _PartNumber != undefined && _StockId != "" && _StockId != null && _StockId != undefined) {
        $.ajax({
            type: "GET",
            url: turl,
            data: { Partnumber: _PartNumber, StockId: _StockId },
            async: false,
            dataType: 'json',
            success: function (data) {
                if (data != null) {  
                    var FromQuantity = data.FromQuantity;
                    $("#FromQuantity").val(FromQuantity);
                } else {                   
                    $("#FromQuantity").val('');
                }

            }
        });
    }
    $("#Ips_loader_div").hide();
}
