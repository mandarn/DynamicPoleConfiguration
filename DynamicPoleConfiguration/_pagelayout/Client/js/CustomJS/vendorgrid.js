﻿$(function () {
    $("#Ips_loader_div").show();
    setTimeout(function () { List(); }, 500);
});


function List() {
    $.ajax({
        type: "Get",
        url: Config.url + 'Administration/Vendor/JsonList',
        dataType: 'json',        
        accept: 'application/json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {            
                data = response;
                createSimpleFilteringGrid();                        
        },
        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            throw err;
        }
    });
    $("#Ips_loader_div").hide();
}

function createSimpleFilteringGrid() {
    $("#gridSimpleFiltering").igGrid({
        autoGenerateColumns: false,
        columns: [
            { headerText: "Vendor Id", key: "VendorId", dataType: "int", hidden: true },
              { headerText: "VendorIdAse", key: "VendorIdAse", dataType: "string", hidden: true },
            { headerText: "Name", key: "VendorName", dataType: "string" },
            { headerText: "Contact Person", key: "ContactPerson", dataType: "string" },
            { headerText: "Contact Number", key: "ContactNumber", dataType: "string" },
            { headerText: "Address", key: "Address", dataType: "string" },
            { headerText: "Action", key: "Action", template: "<a href='" + Config.url + "Administration/PartVendor/Add/${VendorIdAse}'><img title='Add PartVendor' class='button_add_prt tab_sidebar' src='" + Config.url + "Client/img/add.png'/></a> <a href='" + Config.url + "Administration/Vendor/Edit/${VendorIdAse}' class='dwn_btnt'><img title='Edit Vendor' src='" + Config.url + "Client/img/Edit.png'></a> <a href='" + Config.url + "Administration/Vendor/Delete/${VendorIdAse}' class='dwn_btnt'><img title='Delete Vendor' src='" + Config.url + "Client/img/delete.png'></a>", width: "12%" }
            
        ],
        dataSource: data,
        renderCheckboxes: true,
        responseDataKey: "results",
        features: [
            {
                name: "Filtering",
                type: "local",
                mode: "simple",
                filterDialogContainment: "window",
                columnSettings: [
                {
                    columnKey: "VendorName",
                    condition: "startsWith"

                },
                {
                    columnKey: "ContactPerson",
                    condition: "equals"

                },
                 {
                     columnKey: "ContactNumber",
                     condition: "equals"

                 },
                 {
                     columnKey: "Address",
                     condition: "equals"

                 },
                  { columnKey: "Action", allowFiltering: false }
                ]

            },
            {
                name: "Paging",
                type: "local",
                pageSize: 10
            }
        ]
    });
}