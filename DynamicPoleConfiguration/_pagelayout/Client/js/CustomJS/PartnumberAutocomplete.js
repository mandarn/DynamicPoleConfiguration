﻿$("document").ready(function () {
   
    var getData = function (request, response) {
        $.getJSON(
            Config.url + 'Administration/PartVendor/AutoPartNumber?Number=' + request.term,
            function (data) {
                response(data);
            });
    };
    $("#PartNumber").autocomplete({
        source: getData
    });

    //$('#PartNumber').select2();

    
});