﻿$("document").ready(function () {
    var _PartNumber = $("#PartNumber");
    //remove  the validation
    _PartNumber.keyup(function () {
        $("#PartNumberUnique").hide();
        $("#part_btn").attr("disabled", false);
    });

    _PartNumber.blur(function () {
        var getCount = $(".ui-autocomplete li").length;
        if (getCount == 1) {
            $("#PartNumber").val($(".ui-autocomplete li").html());
        }
        else {
            var PartNumber = _PartNumber.val();
            if (PartNumber != "" && PartNumber != null && PartNumber != undefined) {
                $("#PartNumberUnique").hide();
                $("#Ips_loader_div").show();
                setTimeout(function () { getPartNumber(); }, 500);
            }
        }
       
    });   
});



function getPartNumber() {
    var turl = Config.url + 'Administration/Part/Jsonpart';
    var _Number = $("#PartNumber").val();
    if (_Number != "" && _Number != null && _Number != undefined) {
        $.ajax({
            type: "GET",
            url: turl,
            data: { partNumber: _Number },
            async: false,
            dataType: 'json',
            success: function (data) {                
                if (data == true) {
                    $("#PartNumberUnique").show();
                    $("#part_btn").attr("disabled", true);
                }
            }
        });        
    } 
    $("#Ips_loader_div").hide();
}