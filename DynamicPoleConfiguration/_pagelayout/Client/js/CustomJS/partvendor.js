﻿$(document).ready(function () {
    $("form")[0].reset();
    var _Description = $("#Description");
    var _Rate = $("#Rate");
    var _LeadTime = $("#LeadTime");
    var _PartNumber = $("#PartNumber");
  //////////////////////////////////////////////////////////////////////
    _PartNumber.keyup(function () {
        $("#PartNumberUnique").hide();
        _Description.val('');
        _Rate.val('');
        _LeadTime.val('');
    });
      
    //blur off
    _PartNumber.blur(function () {
        var getCount = $(".ui-autocomplete li").length;
        if (getCount == 1) {
            $("#PartNumber").val($(".ui-autocomplete li").html());
        }
        else {
            var partValue = _PartNumber.val();
            if (partValue != "") {
                $("#PartNumberUnique").hide();
                $("#Ips_loader_div").show();
                setTimeout(function () { PartVendorNumber(); }, 5000);
            }
        }
    });
    
});

function PartVendorNumber() {
    var turl = Config.url + 'Administration/PartVendor/Jsonpart';
    var partValue = $("#PartNumber").val();
    var vendorValue = $("#VendorID").val();  
    if (partValue != "" || partValue != null || partValue != undefined) {
        $.ajax({
            type: "GET",
            url: turl,
            data: { partNumber: partValue, vendorID: vendorValue },
            async: false,
            dataType: 'json',
            success: function (data) {               
                $("#part_btn").attr("disabled", false);
                //alert("hello");
                if (data.PartId == null) {
                    //alert("hello");
                    $("#PartNumberUnique").show();
                    $("#part_btn").attr("disabled", true);
                } else {
                    // console.log(data.Description)
                    $("#PartNumberUnique").hide();
                    $("#PartId").html("").val(data.PartId);
                    $("#Description").html("").val(data.Description);
                    $("#Rate").html("").val(data.Rate);
                    $("#LeadTime").html("").val(data.LeadTime);
                }
            },
            error: function (xhr, status, error) {                
                var err = eval("(" + xhr.responseText + ")");
                throw err;
            }
        });       
    } else {       
        $("#Description").val('');
        $("#Rate").val('');
        $("#LeadTime").val('');
    }
    $("#Ips_loader_div").hide();
}