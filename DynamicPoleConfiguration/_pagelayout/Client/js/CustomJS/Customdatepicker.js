﻿$(function () {
    
    var dateToday = new Date();

    $('.custom_datepicker').datepicker({
        format: 'mm/dd/yyyy',
        todayHighlight: true,
        autoclose: true,
    });

    ////disable future date

    $(".custom_datepicker_future").datepicker({
        format: 'mm/dd/yyyy',
        endDate: '+0d',
        autoclose: true
    });


});