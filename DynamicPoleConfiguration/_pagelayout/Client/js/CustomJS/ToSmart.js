$(function () {
    $("#Ips_loader_div").show();
    setTimeout(function () {
        createPurchaseOrderGrid();
        createStockAreaDetailsGrid();
        createPartDetailsGrid();
    }, 500);
    
});

function createPurchaseOrderGrid() {
    $("#gridPurchaseOrder").igGrid({
        autoGenerateColumns: false,
        columns: [
            { headerText: "Purchase Order Number", key: "PurchaseOrderNumber", dataType: "string" },
            { headerText: "Purchase Order Date", key: "PurchaseOrderDate", dataType: "string" },
            { headerText: "Vendor Name", key: "VendorName", dataType: "string" },
            { headerText: "Status", key: "Status", dataType: "string" },
        ],     
        features: [
            {
                name: "Filtering",
                type: "local",
                mode: "simple",
                filterDialogContainment: "window",
                columnSettings: [
                    {
                        columnKey: "PurchaseOrderNumber",
                        condition: "startsWith"

                    },
                    { columnKey: "Action", allowFiltering: false }
                ]

            },
            {
                name: "Paging",
                type: "local",
                pageSize: 10
            }
        ]
    });   
}

function createPartDetailsGrid() {
    $("#gridPartDetails").igGrid({
        autoGenerateColumns: false,
        columns: [
            { headerText: "Stock Area Description", key: "StockAreaDescription", dataType: "string" },
            { headerText: "Part Number", key: "PartNumber", dataType: "string" },
            { headerText: "Part Recieved", key: "PartRecieved", dataType: "string" }           
        ],
        features: [
            {
                name: "Filtering",
                type: "local",
                mode: "simple",
                filterDialogContainment: "window",
                columnSettings: [
                    {
                        columnKey: "PurchaseOrderNumber",
                        condition: "startsWith"

                    },
                    { columnKey: "Action", allowFiltering: false }
                ]

            },
            {
                name: "Paging",
                type: "local",
                pageSize: 10
            }
        ]
    });
}

function createStockAreaDetailsGrid() {
    $("#gridStockAreaDetails").igGrid({
        autoGenerateColumns: false,
        columns: [
            { headerText: "Stock Area Description", key: "StockAreaDescription", dataType: "string" },
            { headerText: "Part Number", key: "PartNumber", dataType: "string" },
            { headerText: "Part Recieved", key: "PartRecieved", dataType: "string" }
        ],
        features: [
            {
                name: "Filtering",
                type: "local",
                mode: "simple",
                filterDialogContainment: "window",
                columnSettings: [
                    {
                        columnKey: "PurchaseOrderNumber",
                        condition: "startsWith"

                    },
                    { columnKey: "Action", allowFiltering: false }
                ]

            },
            {
                name: "Paging",
                type: "local",
                pageSize: 10
            }
        ]
    });
    $("#Ips_loader_div").hide();
}