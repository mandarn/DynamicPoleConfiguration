﻿$(function () {
    var getPartData = function (request, response) {
        var _StockAreaID = $("#StockAreaID").val();

        var _Type = $("#type").val();
        if (_Type == "Select Type") {
            $("#type_error").show();
            return;
        }
        if (_StockAreaID != "") {
            $.getJSON(
                Config.url + 'Transaction/CheckInCheckOut/AutocompletePartNumber?number=' + request.term + '&StockAreaID=' + _StockAreaID,
                function (data) {
                    response(data);
                });
        }
        else {
            $("#stock_error").show();
            return;
        }
    };

    $("#PartNumber").autocomplete({
        source: getPartData
    });



});


function HideAlert(ev)
{
    if (ev.nextElementSibling.style.display != "none")
    {
        ev.nextElementSibling.style.display = "none"
    }
}

//
$(document).ready(function () {
    List();
});
function List() {
    $.ajax({
        type: "Get",
        url: Config.url + 'Transaction/CheckInCheckOut/JsonPartList',
        dataType: 'json',
        accept: 'application/json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            data = response;
            createSimpleFilteringGrid();
        },
        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            throw err;
        }
    });
}

function createSimpleFilteringGrid() {
    $("#gridSimpleFiltering").igGrid({
        autoGenerateColumns: false,
        columns: [
            {
                headerText: "CheckInCheckOutID", key: "CheckInCheckOutID", dataType: "int", hidden: true
            },
             {
                 headerText: "StockAreaID", key: "StockAreaID", dataType: "int", hidden: true
             },
             {
                 headerText: "CheckInCheckOutDetailID", key: "CheckInCheckOutDetailID", dataType: "int", hidden: true
             },
            //{
            //    headerText: "Type", key: "CheckType", dataType: "string"
            //},
            //{
            //    headerText: "Date", key: "Date", dataType: "date"
            //},

            {
                headerText: "Stock Area", key: "StockArea", dataType: "string"
            },
            {
                headerText: "Part Number", key: "PartNumber", dataType: "string"
            },
            {
                headerText: "Quantity", key: "Quantity", dataType: "int"
            },

                  {
                      headerText: "Action", key: "Action", template: "<a id='${CheckInCheckOutDetailID}' style='cursor: pointer;' onclick='Edit(this)' class='dwn_btnt'><img title='Edit RMA' src='" + Config.url + "Client/img/Edit.png'></a> <a href='" + Config.url + "Transaction/CheckInCheckOut/CheckInCheckOutPartDelete/${CheckInCheckOutDetailID}' class='dwn_btnt'><img title='Delete RMA' src='" + Config.url + "Client/img/delete.png'></a>" +
                      "<input class='hidecls' type='hidden' value='${StockAreaID}'/>" +
                      "<input class='hidecls' type='hidden' value='${PartNumber}'/>" +
                      "<input class='hidecls' type='hidden' value='${Quantity}'/>" +
                      "<input  class='hidecls' type='hidden' value='${CheckInCheckOutDetailID}'/>" +
                      "", width: "12%"
                  }

        ],
        dataSource: data,
        renderCheckboxes: true,
        responseDataKey: "results",
        features: [
{
    name: "Filtering",
    type: "local",
    mode: "simple",
    filterDialogContainment: "window",
    columnSettings: [
        {
            columnKey: "PurchaseOrderNumber",
            condition: "startsWith"

        },
{
    columnKey: "Action", allowFiltering: false
}
    ]

},
{
    name: "Paging",
    type: "local",
    pageSize: 10
}
        ]
    });
    $("#Ips_loader_div").hide();
}
var _PartNumber = $("#PartNumber");

_PartNumber.keyup(function () {
    //$("#partnumber_error").hide();
    //$("#part_btn").attr("disabled", false);
    //$(".input_disable").attr("disabled", false);
    //$("#stock_error").hide();
});

//_PartNumber.blur(function () {
//    $("#partnumber_error").hide();
//    var PartNumber = _PartNumber.val();
//    if (PartNumber != "" && PartNumber != null && PartNumber != undefined) {
//        $("#Ips_loader_div").show();
//        setTimeout(function () { getPartnumber(); }, 500);
//    }
//});


function RowAdd() {

    if (checkvalidation()) {

        $("#Ips_loader_div").show();
        if (SelectedRow != null) {
            $(SelectedRow).remove();
        }

        var AppendString = "";
        AppendString += "<tr role='row' tabindex='0'>" +
            "<td role='gridcell' aria-readonly='false' aria-describedby='gridSimpleFiltering_StockArea' tabindex='0' class=''>" + $("#StockAreaID option:selected").text() + "</td>" +
            "<td role='gridcell' aria-readonly='false' aria-describedby='gridSimpleFiltering_PartNumber' tabindex='0' class=''>" + $("#PartNumber").val() + "</td>" +
            "<td role='gridcell' aria-readonly='false' aria-describedby='gridSimpleFiltering_Quantity' tabindex='0' class=''>" + $("#Quantity").val() + "</td>" +
            "<td role='gridcell' aria-readonly='false' aria-describedby='gridSimpleFiltering_Action' tabindex='0' class=''>" +
            "<a id='" + $("#EditCheckPart").val() + "' style='cursor: pointer;' onclick='Edit(this)' class='dwn_btnt'>" +
            "<img title='Edit RMA' src='http://localhost:61092/Client/img/Edit.png'></a>" +
            "<a href='http://localhost:61092/Transaction/CheckInCheckOut/CheckInCheckOutPartDelete/'" + $("#EditCheckPart").val() + " class='dwn_btnt'>" +
            "<img title='Delete RMA' src='http://localhost:61092/Client/img/delete.png'></a>" +
            "<input class='hidecls' type='hidden' value='" + $("#StockAreaID").val() + "'>" +
            "<input class='hidecls' type='hidden' value='" + $("#PartNumber").val() + "'>" +
            "<input class='hidecls' type='hidden' value='" + $("#Quantity").val() + "'>" +
            "<input class='hidecls' type='hidden' value='" + $("#EditCheckPart").val() + "'>" +
            "</td>" +
            "</tr>";
        _recieptDetailID.push($("#EditCheckPart").val());
        _stockAreaID.push($("#StockAreaID").val());
        _partNumber.push($("#PartNumber").val())
        _quantity.push($("#Quantity").val())
        $("#EditCheckPart").val("0")
        $("#StockAreaID").val("")
        $("#PartNumber").val("")
        $("#Quantity").val("")
        $("#BalanceQuantity").val("")
        $(".ui-iggrid-tablebody").append(AppendString);
        $("#Ips_loader_div").hide();
    }
}

var _stockAreaID = new Array();
var _partNumber = new Array();
var _quantity = new Array();
var _recieptDetailID = new Array();
function SavePart() {
      $("#Ips_loader_div").show();
    var jsonarray =
                   {
                       SocketAreaList: _stockAreaID,
                       PartNumberList: _partNumber,
                       QuantityList: _quantity,
                       CheckType: $("#type").val(),
                       CheckDatetime: $("#CheckInCheckOutDate").val(),
                       RecieptDetailID: _recieptDetailID,
                   };


    $.ajax({
        type: "Post",
        url: Config.url + 'Transaction/CheckInCheckOut/SavePartDetails',
        type: 'POST',
        data: "PartDetails=" + JSON.stringify(jsonarray),
        success: function (response) {
            List();
            $("#Ips_loader_div").hide();
        },
        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            throw err;
        }
    });

}

var SelectedRow;
function Edit(ev) {
    $("#Ips_loader_div").show();
    SelectedRow = ev.parentNode.parentNode;
    $("#EditCheckPart").val(ev.id);
    var TD = ev.parentNode;
    var UpdateDetails = $(TD).find(".hidecls")
    $("#StockAreaID").val(UpdateDetails[0].value)
    $("#PartNumber").val(UpdateDetails[1].value)
    $("#PartNumber").blur()
    $("#Quantity").val(UpdateDetails[2].value)
    $("#Ips_loader_div").hide();
}


var _PartNumber = $("#PartNumber");
_PartNumber.keyup(function () {
    $("#partnumber_error").hide();
    $("#part_btn").attr("disabled", false);
    $(".input_disable").attr("disabled", false);
    $("#stock_error").hide();
});

_PartNumber.blur(function () {
    var getCount = $(".ui-autocomplete li").length;
    if (getCount == 1) {
        $("#PartNumber").html($(".ui-autocomplete li")[0].html());

    }
    else {
        $("#partnumber_error").hide();
        var PartNumber = _PartNumber.val();
        if (PartNumber != "" && PartNumber != null && PartNumber != undefined) {
            $("#Ips_loader_div").show();
            setTimeout(function () { getPartnumber(); }, 500);
        }
    }
});

function getPartnumber() {
    var turl = Config.url + 'Transaction/CheckInCheckOut/CheckPartNumber';
    var _PartNumber = $("#PartNumber").val();
    if (_PartNumber != "" && _PartNumber != null && _PartNumber != undefined) {
        $.ajax({
            type: "GET",
            url: turl,
            data: { number: _PartNumber },
            async: false,
            dataType: 'json',
            success: function (data) {

                if (data == true) {
                    $("#partnumber_error").show();
                    $(".input_disable").attr("disabled", true);
                    $("#part_btn").attr("disabled", true);
                    $("#QuantityRequested").val('0');
                    $("#QunatityRemain").val('0');
                    $("#QunatityGet").val('0');
                    //getPartCalculation();
                } else {
                    getPartCalculation();
                }
            }
        });
    }
    $("#Ips_loader_div").hide();
}

function getPartCalculation() {
    var turl = Config.url + 'Transaction/CheckInCheckOut/PartCalculation';
    var _StockAreaID = $("#StockAreaID").val();
    var _PartNumber = $("#PartNumber").val();
    var _CheckType = $("#type").val();
    if (_PartNumber != "" && _PartNumber != null && _PartNumber != undefined && _StockAreaID != "" && _StockAreaID != null && _StockAreaID != undefined) {
        $.ajax({
            type: "GET",
            url: turl,
            data: { stockArea: _StockAreaID, part: _PartNumber, checktype: _CheckType },
            async: false,
            dataType: 'json',
            success: function (data) {
                console.log(data);
                if (data != null) {
                    $("#BalanceQuantity").val('0').val(data.UsedCount);
                    $("#Quantity").val('0').val(data.UsedCount);
                    if (data.RemainCount == 0) { var _remain = 0; } else { var _remain = data.RemainCount; }
                    $("#Quantity").val('0').val(_remain);
                    CheckQuantity();
                }
            }
        });
    }
}


function CheckQuantity() {
    var partNum = $("#PartNumber").val();
    var Date = $("#CheckInCheckOutDate").val();
    var Qty = 0;
    var tbl = $("#gridSimpleFiltering")[0];

    for (var i = 0; i < tbl.rows.length; i++) {
        if (tbl.rows[i].childNodes[1].innerHTML == partNum) {
            Qty = parseInt(tbl.rows[i].childNodes[2].innerHTML);
            SelectedRow = tbl.rows[i].childNodes[2].parentNode;
            $("#EditCheckPart").val(tbl.rows[i].childNodes[3].childNodes[0].id);
        }

    }
    var totalQty = parseInt($("#BalanceQuantity").val()) + Qty;
    var UseQty = totalQty - parseInt($("#Quantity").val());
    $("#BalanceQuantity").val(totalQty);
    $("#Quantity").val(UseQty);
}

var _QunatityRemain = $("#BalanceQuantity");
var _QunatityGet = $("#Quantity");
_QunatityGet.keyup(function () {
    var QunatityRemain = _QunatityRemain.val();
    var QunatityGet = _QunatityGet.val();
    var _status = (Number(QunatityGet) <= Number(QunatityRemain));
    var _zero = (Number(QunatityGet) > Number(0));
    if (_zero == true) {
        if (_status == true) {
            _QunatityGet.val(QunatityGet);
        } else {
            _QunatityGet.val('');
        }
    } else {
        _QunatityGet.val('');
    }

});

function checkvalidation() {
    var status = true;
    $("#type_error").hide()
    $("#stock_error").hide()
    $("#CheckInCheckOutDate_error").hide()
    $("#partnumber_error").hide()
    $("#Quantity_error").hide()
    if ($("#type").val()=="") {
        status = false;
        $("#type_error").show()
    }
    if ($("#StockAreaID").val() == "") {
        status = false;
        $("#stock_error").show()
    }
    if ($("#CheckInCheckOutDate").val() == "") {
        status = false;
        $("#CheckInCheckOutDate_error").show()
    }
    if ($("#PartNumber").val() == "") {
        status = false;
        $("#partnumber_error").show()
    }

    if ($("#Quantity").val() == "" || $("#Quantity").val()=="0") {
        status = false;
        $("#Quantity_error").show()
    }

    
    return status;
}