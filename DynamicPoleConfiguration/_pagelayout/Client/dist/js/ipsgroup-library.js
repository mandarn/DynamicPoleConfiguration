var sampleSectionOutput = "";
var favoritesOutput = "";
var reportsOutput = "";
var editMode = false;
var menuToggle = false;

$(document).ready(function() {

	// Init Build
	buildSampleContent();
	buildReports();
	initFavorites();

	// Init Favorites
	$('.js-favorites-list').append($.parseHTML(favoritesOutput));
	$('.js-favorites-list').sortable({
		revert: 200,
		update: function(event, ui) {
			rebuildFavorites();
		}
	});
	$('.js-favorites-list').sortable('disable');
	$('.js-favorites-list').disableSelection();

	// Init Reports
	$('.js-reports-list').append($.parseHTML(reportsOutput));
	$('.js-reports-list').sortable({
		revert: 200,
		update: function(event, ui) {
			rebuildReports();
		}
	});
	$('.js-reports-list').sortable('disable');
	$('.js-reports-list').disableSelection();

	// Init Sampe List
	$('.js-sample-lists').append($.parseHTML(sampleSectionOutput));

	// Toggle Edit
	$('.js-toggle-edit').click(function() {
		if(editMode) {
			$('.js-favorites-list').sortable('disable');
			$('.js-reports-list').sortable('disable');
			$('.ipsg-main-content').removeClass('state-edit');
			$('.js-toggle-edit').removeClass('icon-font-save');
			$('.js-toggle-eidt').addClass('icon-font-pencil');
			$('.js-toggle-edit').removeClass('state-active');
			editMode = false;
		} else {
			$('.js-favorites-list').sortable('enable');
			$('.js-reports-list').sortable('enable');
			$('.ipsg-main-content').addClass('state-edit');
			$('.js-toggle-eidt').removeClass('icon-font-pencil');
			$('.js-toggle-edit').addClass('icon-font-save');
			$('.js-toggle-edit').addClass('state-active');
			editMode = true;
		}
	});

	// Add Fav
	$('.js-add-fav').click(function() {
		var el       = $(this);
		var parentID = $(this).closest('.ipsg-widget_list').attr('data-id');
		var id       = $(this).parent().attr('data-id');
		var size     = Object.keys(favoritesReports.favorites).length;
		var alert    = '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please remove a favorite before adding a new one.</strong></div>';

		$.each(sampleContent.samples, function(i, sample) {
			if (parentID == sample.sectionID) {
				$.each(sample.sectionLinks, function(j, link) {
					if (id == link.id) {
						var obj = sampleContent.samples[i].sectionLinks[j];

						if(obj.fav) {
							obj.fav = false;
							removeFavoriteElement(parentID+'-'+obj.id, obj.name, obj.url);
							el.removeClass('state-active');
						} else {
							if(size >= 10) {
								$('.ipsg-body').prepend($.parseHTML(alert));
								$('.ipsg-body').animate({ scrollTop: 0 }, 'fast');
								return;
							}

							obj.fav = true;
							addFavoriteElement(parentID+'-'+obj.id, obj.name, obj.url);
							el.addClass('state-active');
						}
					}
				});
			}
		});
	});

	// Init Tabs
	$("#ipsg-tabs").tabs();

	// Init Tooltips
	$( function() {
		$('.ipsg-tab').tooltip();
	});;

	// Init Menu 
	accordionMenu();

	// Toggle Menu
	$('.ipsg-logo-menu-btn').click(function() {
		if(!menuToggle) {
			menuToggle = true;
			$('.sg-accordion > .ipsg-nav-items').hide();
			$('.sg-accordion h3').removeClass('state-selected');
			$('.ipsg-container').addClass('ipsg-menu-collapse');
		} else {
			menuToggle = false;
			$('.ipsg-container').removeClass('ipsg-menu-collapse');
		}
	});

	$('.sample').click(function() {
		console.log(favoritesReports);
	});
});

// Remove Favorite
$(document).on('click', '.js-remove-fav', function() {
	$(this).parent().remove();

	var id  = $(this).closest('.ipsg-reorder-list-item').attr('data-id');
	var loc = id.split("-");
	var el  = $('.ipsg-widget_list[data-id="'+loc[0]+'"]').find('.ipsg-list-item[data-id="'+loc[1]+'"]').find('.ipsg-list-icon');

	el.removeClass('state-active');
	rebuildFavorites();
});

// Remove Report
$(document).on('click', '.js-remove-rep', function() {
	$(this).parent().remove();
	rebuildReports();
});

// Init Menu
function accordionMenu() {
	var allPanels = $('.sg-accordion > .ipsg-nav-items').hide();
	var menuLeaving = false;
	$('.sg-accordion > h3 > a').click(function() {
		if(!menuToggle) {
			if(!$(this).parent().hasClass('state-selected')) {
				allPanels.slideUp('fast');
				$('.sg-accordion h3').removeClass('state-selected');

				$(this).parent().next().slideDown('fast');
				$(this).parent().addClass('state-selected');
			} else {
				allPanels.slideUp('fast');
				$('.sg-accordion h3').removeClass('state-selected');
			}
		}

		return false;
	});

	$('.sg-accordion > h3 > a').mouseenter(function() {
		if(menuToggle) {
			menuLeaving = false;
			$('.sg-accordion > .ipsg-nav-items').hide();
			$('.sg-accordion h3').removeClass('state-selected');
			$(this).parent().next().show();
			$(this).parent().addClass('state-selected');
		}
	});

	$('.sg-accordion > .ipsg-nav-items').mouseenter(function() {
		if(menuToggle) {
			menuLeaving = false;
		}
	});

	$('.sg-accordion > h3 > a').mouseleave(function() {
		if(menuToggle) {
			menuLeaving = true;

			setTimeout(function(){
				if(menuLeaving) {
					$('.sg-accordion > .ipsg-nav-items').hide();
					$('.sg-accordion h3').removeClass('state-selected');
				}
			}, 400);
		}
	});

	$('.sg-accordion > .ipsg-nav-items').mouseleave(function() {
		if(menuToggle) {
			menuLeaving = true;

			setTimeout(function(){
				if(menuLeaving) {
					$('.sg-accordion > .ipsg-nav-items').hide();
					$('.sg-accordion h3').removeClass('state-selected');
				}
			}, 400);
		}
	});
}

// Add Favorite Element
function addFavoriteElement(id, name, url) {
	var html = "";

	html += '<li class="ipsg-reorder-list-item" data-id="'+ id +'">';
		html += '<div class="ipsg-reorder-icon-move icon-thin-0069a_menu_hambuger"></div>';
		html += '<a href="'+ url +'">'+ name +'</a>';
		html += '<div class="ipsg-reorder-icon-remove icon-font-subtract js-remove-fav"></div>';
	html += '</li>';

	$('.js-favorites-list').prepend($.parseHTML(html));

	rebuildFavorites();
}

// Remove Favorite Element
function removeFavoriteElement(id, name, url) {
	$('.js-favorites-list').find('.ipsg-reorder-list-item[data-id="'+id+'"]').remove();

	rebuildFavorites();
}

// Rebuild Favorites Object after Delete
function rebuildFavorites() {
	var size = 0;
	var newFavorites = {
		"favorites": []
	};

	$('.js-favorites-list li').each(function(i) {
		var id     = $(this).attr('data-id');
		var name   = $(this).find('a').text();
		var url    = $(this).find('a').attr('href');
		var newFav = {
			"id"   : id,
			"name" : name,
			"url"  : url
		}

		size = Object.keys(newFavorites.favorites).length;

		if(size <= 9) {
			newFavorites.favorites.splice(size, 0, newFav);
		}
	});

	console.log('Old Favorites List:')
	console.log(favoritesReports);

	console.log('New Favorites List:')
	console.log(newFavorites);

	favoritesReports = newFavorites;
}

// Rebuild Saved Object Reports after Delete
function rebuildReports() {
	var size = 0;
	var newReports = {
		"reports": []
	};

	$('.js-reports-list li').each(function(i) {
		var id     = $(this).attr('data-id');
		var name   = $(this).find('a').text();
		var url    = $(this).find('a').attr('href');
		var newRep = {
			"id"   : id,
			"name" : name,
			"url"  : url
		}

		size = Object.keys(newReports.reports).length;

		if(size <= 9) {
			newReports.reports.splice(size, 0, newRep);
		}
	});

	console.log('Old Saved List:')
	console.log(savedReports);

	console.log('New Saved List:')
	console.log(newReports);

	savedReports = newReports;
}

// Get Initial Favorites 
function initFavorites() {
	var newFavorites = {
		"favorites": []
	};

	$.each(sampleContent.samples, function(i, sample) {
		$.each(sample.sectionLinks, function(j, link) {
			if(link.fav) {
				var id     = sample.sectionID +'-'+ link.id;
				var name   = link.name;
				var url    = link.url;
				var newFav = {
					"id"   : id,
					"name" : name,
					"url"  : url
				}

				size = Object.keys(newFavorites.favorites).length;

				if(size <= 9) {
					newFavorites.favorites.splice(size, 0, newFav);
				}
			}
		});
	});

	favoritesReports = newFavorites;
	buildFavorites();
}

// Starting Favorites Markup
function buildFavorites() {
	$.each(favoritesReports.favorites, function(i, fav) {
		favoritesOutput += '<li class="ipsg-reorder-list-item" data-id="'+ fav.id +'">';
			favoritesOutput += '<div class="ipsg-reorder-icon-move icon-thin-0069a_menu_hambuger"></div>';
			favoritesOutput += '<a href="'+ fav.url +'">'+ fav.name +'</a>';
			favoritesOutput += '<div class="ipsg-reorder-icon-remove icon-font-subtract js-remove-fav"></div>';
		favoritesOutput += '</li>';
	});
}

// Starting Saved Reports Markup
function buildReports() {
	$.each(savedReports.reports, function(i, rep) {
		reportsOutput += '<li class="ipsg-reorder-list-item" data-id="'+ rep.id +'">';
			reportsOutput += '<div class="ipsg-reorder-icon-move icon-thin-0069a_menu_hambuger"></div>';
			reportsOutput += '<a href="'+ rep.url +'">'+ rep.name +'</a>';
			reportsOutput += '<div class="ipsg-reorder-icon-remove icon-font-subtract js-remove-rep"></div>';
		reportsOutput += '</li>';
	});
}

// Stating Sample Content
function buildSampleContent() {
	$.each(sampleContent.samples, function(i, sample) {
		sampleSectionOutput += '<div class="col-xs-12 col-lg-4">';
			sampleSectionOutput += '<div class="ipsg-widget_list" data-id="'+ sample.sectionID +'">';
				sampleSectionOutput += '<div class="ipsg-list-header">' + sample.sectionTitle +'</div>';
				sampleSectionOutput += '<ul class="ipsg-list">';
					$.each(sample.sectionLinks, function(j, link) {
						sampleSectionOutput += '<li class="ipsg-list-item" data-id="'+ link.id +'">';
							if(link.fav) {
								sampleSectionOutput += '<div class="ipsg-list-icon state-active icon-font-star-fill js-add-fav"></div>';
							} else {
								sampleSectionOutput += '<div class="ipsg-list-icon icon-font-star js-add-fav"></div>';
							}
							sampleSectionOutput += '<a href="'+ link.url +'">'+ link.name +'</a>';
						sampleSectionOutput += '</li>';
					});
				sampleSectionOutput += '</ul>';
			sampleSectionOutput += '</div>';
		sampleSectionOutput += '</div>';
	});
}
var sampleContent = {
	"samples": [
		{
			"sectionTitle" : "List 1",
			"sectionID"    : "list1",
			"sectionLinks" : [
				{
					"id"   : "1l1",
					"name" : "1 Item 1",
					"url"  : "#item1",
					"fav"  : true
				},
				{
					"id"   : "1l2",
					"name" : "1 Item 2",
					"url"  : "#item2",
					"fav"  : false
				},
				{
					"id"   : "1l3",
					"name" : "1 Item 3",
					"url"  : "#item3",
					"fav"  : false
				},
				{
					"id"   : "1l4",
					"name" : "1 Item 4",
					"url"  : "#item4",
					"fav"  : false
				},
				{
					"id"   : "1l5",
					"name" : "1 Item 5",
					"url"  : "#item5",
					"fav"  : false
				},
			]
		},
		{
			"sectionTitle" : "List 2",
			"sectionID"    : "list2",
			"sectionLinks" : [
				{
					"id"   : "2l1",
					"name" : "2 Item 1",
					"url"  : "#item1",
					"fav"  : false
				},
				{
					"id"   : "2l2",
					"name" : "2 Item 2",
					"url"  : "#item2",
					"fav"  : true
				},
				{
					"id"   : "2l3",
					"name" : "2 Item 3",
					"url"  : "#item3",
					"fav"  : false
				},
				{
					"id"   : "2l4",
					"name" : "2 Item 4",
					"url"  : "#item4",
					"fav"  : false
				},
				{
					"id"   : "2l5",
					"name" : "2 Item 5",
					"url"  : "#item5",
					"fav"  : false
				},
			]
		},
		{
			"sectionTitle" : "List 3",
			"sectionID"    : "list3",
			"sectionLinks" : [
				{
					"id"   : "3l1",
					"name" : "3 Item 1",
					"url"  : "#item1",
					"fav"  : false
				},
				{
					"id"   : "3l2",
					"name" : "3 Item 2",
					"url"  : "#item2",
					"fav"  : false
				},
				{
					"id"   : "3l3",
					"name" : "3 Item 3",
					"url"  : "#item3",
					"fav"  : false
				},
				{
					"id"   : "3l4",
					"name" : "3 Item 4",
					"url"  : "#item4",
					"fav"  : false
				},
				{
					"id"   : "3l5",
					"name" : "3 Item 5",
					"url"  : "#item5",
					"fav"  : false
				},
			]
		},
		{
			"sectionTitle" : "List 4",
			"sectionID"    : "list4",
			"sectionLinks" : [
				{
					"id"   : "4l1",
					"name" : "4 Item 1",
					"url"  : "#item1",
					"fav"  : false
				},
				{
					"id"   : "4l2",
					"name" : "4 Item 2",
					"url"  : "#item2",
					"fav"  : false
				},
				{
					"id"   : "4l3",
					"name" : "4 Item 3",
					"url"  : "#item3",
					"fav"  : false
				},
				{
					"id"   : "4l4",
					"name" : "4 Item 4",
					"url"  : "#item4",
					"fav"  : false
				},
				{
					"id"   : "4l5",
					"name" : "4 Item 5",
					"url"  : "#item5",
					"fav"  : false
				},
			]
		}
	]
}

var favoritesReports = {
	"favorites": [
		// {
		// 	"id"   : "fav1",
		// 	"name" : "Engineer Best-of-breed",
		// 	"url"  : "#fav1"
		// },
		// {
		// 	"id"   : "fav2",
		// 	"name" : "Rss-capable Best-of-breed",
		// 	"url"  : "#fav2"
		// },
		// {
		// 	"id"   : "fav3",
		// 	"name" : "Repurpose Turn-key",
		// 	"url"  : "#fav3"
		// },
		// {
		// 	"id"   : "fav4",
		// 	"name" : "Reinvent Streamline",
		// 	"url"  : "#fav4"
		// },
		// {
		// 	"id"   : "fav5",
		// 	"name" : "Customized E-commerce",
		// 	"url"  : "#fav5"
		// },
		// {
		// 	"id"   : "fav6",
		// 	"name" : "Open-source Tagclouds",
		// 	"url"  : "#fav6"
		// },
		// {
		// 	"id"   : "fav7",
		// 	"name" : "Long-tail Bricks-and-clicks",
		// 	"url"  : "#fav7"
		// },
		// {
		// 	"id"   : "fav8",
		// 	"name" : "User-centred Web-readiness",
		// 	"url"  : "#fav8"
		// },
		// {
		// 	"id"   : "fav9",
		// 	"name" : "E-markets Efficient",
		// 	"url"  : "#fav9"
		// },
		// {
		// 	"id"   : "fav10",
		// 	"name" : "Supply-chains Enable",
		// 	"url"  : "#fav10"
		// }
	]
}

var savedReports = {
	"reports": [
		{
			"id"   : "rep1",
			"name" : "Report 1",
			"url"  : "#rep1"
		},
		{
			"id"   : "rep2",
			"name" : "Report 2",
			"url"  : "#rep2"
		},
		{
			"id"   : "rep3",
			"name" : "Report 3",
			"url"  : "#rep3"
		},
		{
			"id"   : "rep4",
			"name" : "Report 4",
			"url"  : "#rep4"
		},
		{
			"id"   : "rep5",
			"name" : "Report 5",
			"url"  : "#rep5"
		},
		{
			"id"   : "rep6",
			"name" : "Report 6",
			"url"  : "#rep6"
		},
		{
			"id"   : "rep7",
			"name" : "Report 7",
			"url"  : "#rep7"
		},
		{
			"id"   : "rep8",
			"name" : "Report 8",
			"url"  : "#rep8"
		},
		{
			"id"   : "rep9",
			"name" : "Report 9",
			"url"  : "#rep9"
		},
		{
			"id"   : "rep10",
			"name" : "Report 10",
			"url"  : "#rep10"
		}
	]
}
